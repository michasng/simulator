# Simulation Framework

A framework for running and visualizing simulations. The resulting visualizations are described in a data-driven manner using JSON files. These files operate similar to CSS for HTML, but they're not restricted to any specific format.

## Getting Started

### Dependencies

* Python 3.8.6 (only tested version)
* pip

### Installing

* Install requirements

```
pip install -r requirements.txt
```

* Install the project locally in editable mode
```
pip install -e .
```

### Executing program

* Navigate to a sample simulation

```
cd simulator\samples\infectious_disease
```

* Run the sample

```
python infectious_disease.py
```

## Help

Any advise regarding the usage of the framework may be found in the thesis.

## Authors

Micha Sengotta
micha.sengotta@gmail.com
