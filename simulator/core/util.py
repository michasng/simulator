import math, random, statistics

def feed_forward(result):
  return result

def feed_forward_index(index, results):
  return results[index]

_comparators = {
  '==': lambda v1, v2 : v1 == v2,
  '!=': lambda v1, v2 : v1 != v2,
  '>': lambda v1, v2 : v1 > v2,
  '>=': lambda v1, v2 : v1 >= v2,
  '<': lambda v1, v2 : v1 < v2,
  '<=': lambda v1, v2 : v1 <= v2,
}
def feed_forward_if(result, v1, comp, v2, else_result=None):
  comparator = _comparators.get(comp)
  if comparator is None:
    raise ValueError(f'Comparator not defined {comp}.')
  if comparator(v1, v2):
    return result
  return else_result

def filter_nan(numbers):
  return [n for n in numbers if not math.isnan(n)]

def map(v, v_min, v_max, target_min, target_max):
  norm = (v - v_min) / (v_max - v_min)
  return norm * (target_max - target_min) + target_min

def linear_interpolate(origin, target, progress):
  return origin + (target - origin) * progress

def linear_interpolate_anything(origin, target, progress):
  # print(f'interpolate anything: {origin} at {progress} to {target}')
  if origin is None:
    return None
  if type(origin) is list or type(origin) is tuple: # recursive
    if len(origin) != len(target):
      if progress == 1:
        return target
      return origin
    return [linear_interpolate_anything(origin[i], target[i], progress) for i in range(len(origin))]
  if type(origin) is str:
    if progress < 0.5:
      return origin
    return target
  return linear_interpolate(origin, target, progress)

def randnum(min, max, rd=random):
  return map(rd.random(), 0, 1, min, max)

# slightly modified version of:
# https://code.activestate.com/recipes/511478-finding-the-percentile-of-the-values/
def percentile(data, percent):
  '''
  Find the percentile of a list of values.

  @parameter data - is a list of values.
  @parameter percent - a float value from 0.0 to 1.0.
  @parameter key - optional key function to compute value from each element of N.

  @return - the percentile of the values
  '''
  if not data:
    return None
  data.sort()
  k = (len(data)-1) * percent
  f = math.floor(k)
  c = math.ceil(k)
  if f == c:
      return data[int(k)]
  d0 = data[int(f)] * (c-k)
  d1 = data[int(c)] * (k-f)
  return d0+d1


'''
Wraps multiple lists in an object with an index property.

@parameter args - lists with l[0] for the property name and l[1] for its values.

@return - a list of objects with the index and args properties.
'''
def wrap_with_index(*args):
  # assert len(args[i][1]) == len(args[i+1][1])
  NAME = 0
  VALUES = 1
  array_length = len(args[0][VALUES])
  result = []
  for index in range(array_length):
    obj = { 'index': index }
    for arg in args:
      obj[arg[NAME]] = arg[VALUES][index]
    result.append(obj)
  return result

def add_index_field(list):
  for index, element in enumerate(list):
    element['index'] = index

def unroll_matrix(matrix, create_id = False):
  unrolled = []
  for row_idx, row in enumerate(matrix):
    for col_idx, value in enumerate(row):
      unrolled.append({
        'row': row_idx,
        'col': col_idx,
        'value': value,
      })
  if create_id:
    for cell in unrolled:
      cell['id'] = cell['row'] * len(matrix) + cell['col']
  return unrolled

def set_default_values(context, *key_values):
  for key_value in key_values:
    key, value = key_value[0], key_value[1]
    if len(key_value) == 2:
      if key not in context:
        context[key] = value
    elif len(key_value) == 4:
      list_key, length = key_value[2], key_value[3]
      if list_key not in context:
        if key not in context:
          context[key] = value
        context[list_key] = [context[key] for i in range(length)]
    else:
      raise ValueError(f'key_value [key, value, ?list_key, ?length] must be of length 2 or 4: {key_value}.')

def copy_arg_in_list(*args):
  '''
  Copy a specific argument from each element in a list to another element.

  @parameter array - the list to copy args in.
  @parameter offset - the index-offset between the elements.
  @parameter origin_name - the name of the variable to copy from.
  @parameter target_name - the name of the variable to copy to.

  @return - the modified list (but it is edited in-place anyway)
  '''
  array = args[0]
  offset = args[1]
  origin_name = args[2]
  target_name = args[3]
  for i in range(len(array)):
    if i + offset < 0 or i + offset >= len(array):
      continue
    array[i][target_name] = array[i + offset][origin_name]
  return array
