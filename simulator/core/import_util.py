import os, importlib.util

# pydoc.locate checks in the directory of the executing script, but I need to check in the cwd
def import_property(module_name, property_name, dir = '.'):
  module_path = os.path.join(dir, f'{module_name}.py')

  # dynamically importing the module
  spec = importlib.util.spec_from_file_location(module_name, module_path)
  module = importlib.util.module_from_spec(spec)
  spec.loader.exec_module(module)

  # dynamically instantiating the class
  prop = getattr(module, property_name)
  return prop
