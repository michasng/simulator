import random, math
import numpy as np
from simulator.core.util import map, randnum

def vec(*args):
  return np.array(args, dtype=object)

def randunitvec(rd=random):
  phi = 2 * math.pi * rd.random()
  return vec(math.cos(phi), math.sin(phi))

def randvec(lower_limit, upper_limit, rd=random):
  unitvec = randunitvec(rd)
  radius = randnum(lower_limit, upper_limit, rd)
  return unitvec * radius

def dot(v1, v2):
  return np.dot(v2, v2)

def magnitude(v):
  return np.linalg.norm(v)

def normalize(v):
  mag = magnitude(v)
  if mag == 0: 
    return v
  return v / mag

