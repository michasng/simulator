from simulator.config import get_app_config

class Simulator:
  def __init__(self, config, simulation):
    self.config = config
    self.simulation = simulation

  def init(self):
    if get_app_config()['debug']:
      print('start simulation')
    self.step_count = 0
    initial_states = self.simulation.init(get_app_config()['params'])
    # print(f'initial states: {initial_states}')
    if initial_states:
      self.state = initial_states[-1]
    else:
      initial_states = []
      self.state = None
    return initial_states

  def update_batch(self):
    if self.config['batch_size'] == 0:
      # run all steps at once
      batch_size = self.config['total_steps']
    else:
      # run for batch_size, but never more than is missing
      batch_size = min(self.config['batch_size'], self.missing_steps())

    if get_app_config()['debug']:
      print(f'update batch (size: {batch_size})')

    new_states = self.simulation.update_batch(self.config['step_size'], self.state, batch_size)
    self.state = new_states[-1]
    self.step_count += len(new_states)
    return new_states

  def run_all(self):
    states = self.init()
    while self.missing_steps() > 0:
      states += self.update_batch()
    self.quit()
    return states

  def missing_steps(self):
    return self.config['total_steps'] - self.step_count

  def on_write_state_file(self, index, state):
    return self.simulation.on_write_state_file(index, state)

  def quit(self):
    self.simulation.quit()
    if get_app_config()['debug']:
      print('stop simulation')
