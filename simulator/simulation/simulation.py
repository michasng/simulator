class Simulation():

  '''
  Initializes the simulation.

  @parameter params - a custom object from the config.

  @return - a list of state-objects that should exist before the update cycle begins.
  '''
  def init(self, params):
    raise NotImplementedError

  '''
  Computes a new state.

  @parameter step_size - the size of the time_step (defaults to 1).
  @parameter old_state - the last computed state (if any).

  @return - a state object.
  '''
  def update(self, step_size, old_state):
    raise NotImplementedError

  '''
  Computes multiple new states in a batch. Override this if states can be computed in parallel.

  @parameter step_size - the size of the time_step (defaults to 1).
  @parameter old_state - the last computed state (if any).
  @parameter batch_size - the amount of new state to compute.

  @return - a list of state-objects.
  '''
  def update_batch(self, step_size, old_state, batch_size):
    temp_state = old_state
    new_states = []
    for i in range(batch_size):
      temp_state = self.update(step_size, temp_state)
      new_states.append(temp_state)
    return new_states

  '''
  Is called once for every state (from init or update). Can be used to export the state to different file formats.

  @parameter index - the index of the state in the list of states.
  @parameter state - the state to be written.

  @return - a boolean value indicating whether writing a file should be prevented.
  '''
  def on_write_state_file(self, index, state):
    return False

  '''
  Is called to clean up (e.g. after multiprocessing)
  '''
  def quit(self):
    pass
