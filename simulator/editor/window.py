import pyglet

# based on https://github.com/adamlwgriffiths/Pyglet/blob/master/examples/text_input.py
class Window(pyglet.window.Window):
  def __init__(self, *args, **kwargs):
    super(Window, self).__init__(*args, **kwargs)

    self.batch = pyglet.graphics.Batch()
    self.widgets = []
    self.text_cursor = self.get_system_mouse_cursor('text')

    self.focus = None

  def add_widgets(self, *widgets):
    self.widgets += widgets

  def on_resize(self, width, height):
    super(Window, self).on_resize(width, height)

  def on_draw(self):
    pyglet.gl.glClearColor(1, 1, 1, 1)
    self.clear()
    self.batch.draw()

  def on_mouse_motion(self, x, y, dx, dy):
    for widget in self.widgets:
      if widget.hit_test(x, y):
        self.set_mouse_cursor(self.text_cursor)
        break
    else:
        self.set_mouse_cursor(None)

  def on_mouse_press(self, x, y, button, modifiers):
    for widget in self.widgets:
      if widget.hit_test(x, y):
        if widget.focusable:
          self.set_focus(widget)
        else:
          self.set_focus(None)
        break
    else:
      self.set_focus(None)

    if self.focus:
      self.focus.on_mouse_press(x, y, button, modifiers)

  def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
    if self.focus:
      self.focus.on_mouse_drag(x, y, dx, dy, buttons, modifiers)

  def on_text(self, text):
    if self.focus:
      self.focus.on_text(text)

  def on_text_motion(self, motion):
    if self.focus:
      self.focus.on_text_motion(motion)
    
  def on_text_motion_select(self, motion):
    if self.focus:
      self.focus.on_text_motion_select(motion)

  def on_mouse_scroll(self, x, y, scroll_x, scroll_y):
    scroll_widget = None
    for widget in self.widgets:
      if widget.hit_test(x, y):
        scroll_widget = widget
        break
    if not scroll_widget:
      return
    
    scale = 10
    scroll_widget.on_scroll(scroll_x * scale, scroll_y * scale)

  def on_key_press(self, symbol, modifiers):
    if symbol == pyglet.window.key.TAB:
      if modifiers & pyglet.window.key.MOD_SHIFT:
        dir = -1
      else:
        dir = 1

      if self.focus in self.widgets:
        i = self.widgets.index(self.focus)
      else:
        i = 0
        dir = 0

      for j in range(len(self.widgets)): # find the next focusable one
        new_focus = self.widgets[(i + dir) % len(self.widgets)]
        if new_focus.focusable:
          break
        i += 1
      if not new_focus.focusable:
        new_focus = None

      self.set_focus(new_focus)

    elif symbol == pyglet.window.key.ESCAPE:
      pyglet.app.exit()

  def set_focus(self, focus):
    if self.focus:
      self.focus.set_focused(False)

    self.focus = focus
    if self.focus:
      self.focus.set_focused(True)
