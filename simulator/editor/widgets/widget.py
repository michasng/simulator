class Widget(object):
  def __init__(self, focusable):
    self.focusable = focusable

  def hit_test(self, x, y):
    raise NotImplementedError()

  def on_scroll(self, scroll_x, scroll_y):
    raise NotImplementedError()

  def set_focused(self, is_focused):
    raise RuntimeError('Cannot focus unfocusable widget.')
