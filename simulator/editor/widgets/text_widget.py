import pyglet
from simulator.editor.widgets.widget import Widget

class TextWidget(Widget):
  def __init__(self, batch, text, x, y, width, height=18):
    super().__init__(focusable=True)
    
    self.document = pyglet.text.document.UnformattedDocument(text)
    self.document.set_style(0, len(self.document.text), dict(color=(0, 0, 0, 255)))

    # displayed text suitable for interactive editing and/or scrolling large documents
    self.layout = pyglet.text.layout.IncrementalTextLayout(
      self.document, width, height, multiline=True, batch=batch
    )
    self.layout.x = x
    self.layout.y = y
    
    # caret = text insertion marker
    self.caret = pyglet.text.caret.Caret(self.layout)

    # Rectangular outline
    pad = 2
    self.rectangle = pyglet.shapes.Rectangle(
      x - pad,
      y - pad,
      width + 2 * pad,
      height + 2 * pad,
      [200, 200, 220],
      batch,
    )

    self.on_text_observer = None

  def hit_test(self, x, y):
    return (0 < x - self.layout.x < self.layout.width and
            0 < y - self.layout.y < self.layout.height)

  def on_scroll(self, scroll_x, scroll_y):
    self.layout.view_x += scroll_x

  def set_focused(self, is_focused):
    if is_focused:
      self.caret.visible = True
      self.caret.mark = 0
      self.caret.position = len(self.document.text)
    else:
      self.caret.visible = False
      self.caret.mark = self.caret.position = 0

  def on_mouse_press(self, x, y, button, modifiers):
    self.caret.on_mouse_press(x, y, button, modifiers)

  def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
    self.caret.on_mouse_drag(x, y, dx, dy, buttons, modifiers)

  def on_text(self, text):
    self.caret.on_text(text)
    if self.on_text_observer:
      self.on_text_observer(self, self.document.text)

  def on_text_motion(self, motion):
    self.caret.on_text_motion(motion)
    
  def on_text_motion_select(self, motion):
    self.caret.on_text_motion_select(motion)
