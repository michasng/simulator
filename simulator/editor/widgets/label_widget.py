import pyglet
from simulator.editor.widgets.widget import Widget

class LabelWidget(Widget):
  def __init__(self, batch, text, x, y, width, height=18, *args, **kwargs):
    super().__init__(focusable=False)

    self.document = pyglet.text.document.UnformattedDocument(text)
    self.document.set_style(0, len(self.document.text), dict(color=(0, 0, 0, 255)))
    
    if height == 18:
      self.layout = pyglet.text.layout.TextLayout(
        self.document, width, height, multiline=False, batch=batch
      )
    else:
      self.layout = pyglet.text.layout.ScrollableTextLayout(
        self.document, width, height, multiline=True, batch=batch
      )
      # Rectangular outline
      pad = 2
      x1 = x - pad
      y1 = y - pad
      x2 = x1 + width + 2 * pad
      y2 = y1 + height + 2 * pad
      self.rectangle = [
        pyglet.shapes.Line(x1, y1, x1, y2, width=1, color=(200, 200, 200), batch=batch),
        pyglet.shapes.Line(x1, y2, x2, y2, width=1, color=(200, 200, 200), batch=batch),
        pyglet.shapes.Line(x2, y2, x2, y1, width=1, color=(200, 200, 200), batch=batch),
        pyglet.shapes.Line(x2, y1, x1, y1, width=1, color=(200, 200, 200), batch=batch),
      ]

    self.layout.x = x
    self.layout.y = y

  def hit_test(self, x, y):
    return (0 < x - self.layout.x < self.layout.width and
            0 < y - self.layout.y < self.layout.height)

  def on_scroll(self, scroll_x, scroll_y):
    if self.layout.height > 18:
      self.layout.view_x += scroll_x
      self.layout.view_y += scroll_y
