import json, pyglet
from simulator.editor.widgets.text_widget import TextWidget
from simulator.editor.widgets.label_widget import LabelWidget
from simulator.editor.window import Window
from simulator.config.validator import validate_json

window = Window(1080, 720, caption='Editor', resizable=True)

p = 10 # padding around all widgets
g = 20 # gap between widgets
w = window.width / 2 - p * 2 - g / 2 # average width
h = window.height / 2 - p * 2 - g / 2 # average height
hl = h + h / 2
hs = h / 2

config_label = LabelWidget(window.batch, 'Config', p, p+hs+g+hl, w)
config_edit = TextWidget(window.batch, '{\r\n}', p, p+hs+g, w, hl)
state_label = LabelWidget(window.batch, 'State', p+w+g, p+hl+g+hs, w)
state_edit = TextWidget(window.batch, '{\r\n}', p+w+g, p+hl+g, w, hs)
console_out = LabelWidget(window.batch, 'Output', p, p, w, hs)
preview = LabelWidget(window.batch, 'Preview', p+w+g, p, w, hl)

window.add_widgets(config_label, config_edit, state_label, state_edit, console_out, preview)

def on_text(widget, text):
  # print(text)
  try:
    app_config = json.loads(text)
    validate_json(app_config, 'app.schema')
    console_out.document.text = 'Config valid'
  except Exception as error:
    console_out.document.text = str(error)

config_edit.on_text_observer = on_text

pyglet.app.run()
