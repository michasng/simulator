import random, math
from simulator.core.util import *
from simulator.core.vector import *
from simulator.simulator import simulate
from simulator.simulation.simulation import Simulation
from simulator.visuals.color import *

class ParticleSystem(Simulation):

  def init(self, params):
    self.params = params
    self.ticks = 0
    self.max_id = -1

    state = []
    for i in range(self.params['start_count']):
      state.append(self.create_particle())
    return [state]

  '''
  each particle requires:
  - initial position
  - initial velocity (both speed and direction)
  - initial size
  - initial color
  - initial transparency
  - shape
  - lifetime
  '''
  def create_particle(self):
    spread = randvec(-self.params['spread'], self.params['spread'])
    pos = vec(*self.params['position']) + spread
    vel = vec(*self.params['velocity'])
    vel = vel + randvec(-self.params['velocity_randomness'], self.params['velocity_randomness'])
    color = hsv_to_rgb(randnum(0, 50), 1, randnum(0.8, 1))
    lifetime_randomness = randnum(-self.params['lifetime_randomness'], self.params['lifetime_randomness'])
    lifetime = self.params['lifetime'] + lifetime_randomness
    self.max_id += 1
    return {
      'pos': pos,
      'vel': vel,
      'size': 5,
      'color': color,
      'opacity': 1,
      'lifetime': lifetime,
      'birthtime': self.ticks,
      'id': self.max_id,
    }

  def update(self, step_size, old_state):
    self.ticks += step_size

    state = []
    # print(f'{self.ticks}: {state}')
    # print()
    for particle in old_state:
      life_progress = (self.ticks - particle['birthtime']) / particle['lifetime']
      if life_progress >= 1:
        continue # not re-adding = deleting the particle
      pos = particle['pos'] + particle['vel'] * step_size
      opacity = 1 - life_progress
      state.append({
          'pos': pos,
          'vel': particle['vel'],
          'size': self.params['size'],
          'color': particle['color'],
          'opacity': opacity,
          'lifetime': particle['lifetime'],
          'birthtime': particle['birthtime'],
          'id': particle['id'],
        }
      )
    
    if self.ticks % self.params['spawn_rate'] == 0:
      # spawn particle
      state.append(self.create_particle())

    return state

if __name__ == '__main__':
  simulate(ParticleSystem())
