from simulator.simulator import simulate
from simulator.simulation.simulation import Simulation

class LayoutTest(Simulation):

  def init(self, params):
    return [{}]

  def update(self, step_size, old_state):
    return {}

if __name__ == '__main__':
  simulate(LayoutTest())
