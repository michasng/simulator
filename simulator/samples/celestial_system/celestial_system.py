import math, time
from simulator.core.util import randnum
from simulator.core.vector import *
from simulator.simulator import simulate, visualize
from simulator.simulation.simulation import Simulation
import numpy as np

def get_pixel_radius(body, pixel_size, space_size, multiplier):
  radius = mass_to_radius(body['mass'], body['density'])
  space_to_pixel = pixel_size / space_size
  scaled_radius = radius * space_to_pixel * multiplier
  if scaled_radius < 4: # so it is still visible
    return 4
  return scaled_radius

def mass_to_radius(mass, density):
  # 1 g/cm³ = 1000000 g/m³ = (/1000) 1000 kg/m³
  # 1 g/cm³ = 3.34792898e+39 g/au³ = (/1.989e+33) 1683222.21217 Mo/au³
  unit_corrected_density = density * 1683222.21217
  volume = mass / unit_corrected_density
  return math.pow((3 * volume) / (4 * math.pi), 1/3)

# using astronomical units for distance, solar masses for mass and years for time
class CelestialSystem(Simulation):

  def init(self, params):
    self.updates_per_step = params['updates_per_step']
    self.gravitational_constant = params['gravitational_constant']
    self.space_size = params['space_size']
    self.terminal_velocity = params['terminal_velocity']
    self.num_cells = params['num_cells'] # cells for collision optimization
    self.cell_size = self.space_size / self.num_cells
    self.clear_cells()

    seed = params['seed'] if 'seed' in params else int(time.time())
    print(f'using seed: {seed}')
    random.seed(seed)

    bodies = []
    self.min_random_mass = params['min_random_mass']
    self.max_random_mass = params['max_random_mass']
    self.min_random_density = params['min_random_density']
    self.max_random_density = params['max_random_density']
    self.max_random_velocity = params['max_random_velocity']
    for i in range(params['random_bodies']):
      mass = randnum(self.min_random_mass, self.max_random_mass)
      density = randnum(self.min_random_density, self.max_random_density)
      # random point on a square plane
      pos = vec(randnum(0, self.space_size), randnum(0, self.space_size))
      # random direction with random magnitude (random point on a circular plane)
      vel = randvec(0, self.max_random_velocity)
      body = {
        'id': i,
        'mass': mass,
        'density': density, # material density in g/cm³
        'pos': pos,
        'vel': vel,
      }
      bodies.append(body)
      self.add_body_to_cell(body)

    # self.print_cells()
    # just in case two positions are equal from the start
    self.handle_collisions(bodies)

    return [{
      'bodies': bodies,
    }]

  def clear_cells(self):
    self.cells = []
    for row_idx in range(self.num_cells):
      row = []
      for col_idx in range(self.num_cells):
        row.append([])
      self.cells.append(row)

  def add_body_to_cell(self, body):    
    body['cell_row'] = int(body['pos'][0] / self.cell_size)
    body['cell_col'] = int(body['pos'][1] / self.cell_size)
    self.cells[body['cell_row']][body['cell_col']].append(body)

  def remove_body_from_cell(self, body):
    self.cells[body['cell_row']][body['cell_col']].remove(body)
    del body['cell_row']
    del body['cell_col']

  def get_adjacent_cells(self, body):
    adjacent = []
    for row_offset in range(-1, 2):
      for col_offset in range(-1, 2):
        row = (body['cell_row'] + row_offset) % self.num_cells
        col = (body['cell_col'] + col_offset) % self.num_cells
        adjacent.append(self.cells[row][col])
    self.remove_body_from_cell(body) # this body is no longer considered for collisions
    return adjacent

  def print_cells(self):
    s = ''
    for row in range(self.num_cells):
      s += '['
      for col in range(self.num_cells):
        s += f'[{len(self.cells[row][col])}]'
      s += ']'
    print(s)

  def get_distance_vector(self, p1, p2):
    # in euclidean space:
    euclidian_distance_vector = p2 - p1
    # in non-euclidean space:
    constrained_vector = euclidian_distance_vector % self.space_size
    if constrained_vector[0] > self.space_size / 2:
      constrained_vector[0] = constrained_vector[0] - self.space_size
    if constrained_vector[1] > self.space_size / 2:
      constrained_vector[1] = constrained_vector[1] - self.space_size
    return constrained_vector

  def move_bodies(self, step_size, old_bodies):
    bodies = []
    for body in old_bodies: # deep copy
      body = {
        'id': body['id'],
        'mass' : body['mass'],
        'density' : body['density'],
        'pos' : body['pos'],
        'vel' : body['vel'],
        'force': vec(0, 0), # for temporary calculations
      }
      bodies.append(body)
      self.add_body_to_cell(body)

    # attraction
    for index, entity in enumerate(bodies):
      # compare bodies only with those after it in the list
      for other_index in range(index + 1, len(bodies)):
        other = bodies[other_index]
        distance = self.get_distance_vector(entity['pos'], other['pos'])
        distance_mag = magnitude(distance)
        distance_mag_squared = distance_mag * distance_mag
        direction = normalize(distance)
        force = direction * self.gravitational_constant * entity['mass'] * other['mass'] / distance_mag_squared
        entity['force'] += force
        other['force'] -= force

      acceleration = entity['force'] / entity['mass']
      # consider timestep size
      entity['vel'] += acceleration * step_size
      
      if self.terminal_velocity > -1 and magnitude(entity['vel']) > self.terminal_velocity:
        entity['vel'] = normalize(entity['vel'])
        entity['vel'] = entity['vel'] * self.terminal_velocity
      
      entity['pos'] += entity['vel'] * step_size
      entity['pos'] %= self.space_size # constrain space

    return bodies

  def handle_collisions(self, bodies):
    collisions = []
    # collision detection
    for entity in bodies:
      entity_radius = mass_to_radius(entity['mass'], entity['density'])
      # compare bodies only with adjacent grid cells
      for cell in self.get_adjacent_cells(entity):
        for other in cell:
          if other is entity:
            continue
          distance = magnitude(entity['pos'] - other['pos'])
          other_radius = mass_to_radius(other['mass'], other['density'])
          if distance < entity_radius + other_radius:
            collisions.append([entity, other])
            # in case more than two bodies collide at once:
            overlapping_collisions = []
            for c in collisions:
              if entity in c or other in c:
                overlapping_collisions.append(c)
            if len(overlapping_collisions) > 1:
              # combine all overlaps
              new_collision = []
              for c in overlapping_collisions:
                collisions.remove(c)
                for e in c:
                  if e not in new_collision:
                    new_collision.append(e)
              collisions.append(new_collision)

    # remove collided bodies
    for collision in collisions:
      total_mass = 0
      for entity in collision:
        bodies.remove(entity)
        total_mass += entity['mass']

      # combine them to a new body
      weighted_average_pos = vec(0, 0)
      weighted_average_vel = vec(0, 0)
      weighted_average_density = 0
      most_massive = collision[0]
      for entity in collision:
        mass_percentage = entity['mass'] / total_mass
        weighted_average_pos += entity['pos'] * mass_percentage
        weighted_average_vel += entity['vel'] * mass_percentage
        weighted_average_density += entity['density'] * mass_percentage
        if entity['mass'] > most_massive['mass']:
          most_massive = entity
      bodies.append({
        'id': most_massive['id'],
        'mass' : total_mass,
        'density': weighted_average_density,
        'pos' : weighted_average_pos,
        'vel' : weighted_average_vel,
      })

  def update(self, step_size, old_state):
    bodies = old_state['bodies']
    for i in range(self.updates_per_step):
      bodies = self.move_bodies(step_size, bodies)
      self.handle_collisions(bodies) # will modify the list in-place
    return {
      'bodies': bodies,
    }

if __name__ == '__main__':
  simulate(CelestialSystem())
  # visualize()
