import numpy as np
from simulator.simulator import simulate
from simulator.simulation.simulation import Simulation

def analyse_distribution(values, start, stop, step):
  sorted_values = sorted(values)
  counts = []
  top_index = 0
  for x in np.arange(start, stop, step):
    count = 0
    for index in range(top_index, len(sorted_values)):
      if sorted_values[index] < x:
        count += 1
      else:
        top_index = index
        break
    counts.append(count)
  return counts

class NormalDistribution(Simulation):

  def init(self, params):
    self.loc = params['loc']
    self.scale = params['scale']
    return []

  def update(self, step_size, old_state):
    r = np.random.normal(self.loc, self.scale)
    return {
      "value": r
    }

if __name__ == '__main__':
  simulate(NormalDistribution())
