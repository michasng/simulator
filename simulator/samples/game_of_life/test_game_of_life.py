from simulator.samples.game_of_life.game_of_life import GameOfLife

if __name__ == '__main__':
  gol = GameOfLife()
  
  assert not gol.determine_value(False, 0)
  assert not gol.determine_value(False, 1)
  assert not gol.determine_value(False, 2)
  assert gol.determine_value(False, 3)
  assert not gol.determine_value(False, 4)
  assert not gol.determine_value(False, 5)
  assert not gol.determine_value(False, 6)
  assert not gol.determine_value(False, 7)
  assert not gol.determine_value(False, 8)
  
  assert not gol.determine_value(True, 0)
  assert not gol.determine_value(True, 1)
  assert gol.determine_value(True, 2)
  assert gol.determine_value(True, 3)
  assert not gol.determine_value(True, 4)
  assert not gol.determine_value(True, 5)
  assert not gol.determine_value(True, 6)
  assert not gol.determine_value(True, 7)
  assert not gol.determine_value(True, 8)
