import random, time
from simulator.simulator import simulate
from simulator.simulation.simulation import Simulation

def remove_false_cells(unrolled_matrix):
  return [cell for cell in unrolled_matrix if cell['value']]

class GameOfLife(Simulation):

  def init(self, params):
    num_cols, num_rows = params['num_cols'], params['num_rows']
    if params['pattern'] == 'x':
      matrix = self.make_pattern(num_cols, num_rows, self.make_cell_x)
    elif params['pattern'] == 'random':
      seed = params['seed'] if 'seed' in params else int(time.time())
      print(f'using seed: {seed}')
      random.seed(seed)
      matrix = self.make_pattern(num_cols, num_rows, self.make_cell_random)
    else:
      raise ValueError(f'pattern unknown {params["pattern"]}.')

    # Moore neighborhood mask
    self.mask = []
    for row in range(-1, 2):
      for col in range(-1, 2):
        if not (col == 0 and row == 0):
          self.mask.append([col, row])
    
    return [{'matrix': matrix}]

  def make_pattern(self, num_cols, num_rows, cell_factory):
    matrix = []
    for row_idx in range(num_rows):
      row = []
      for col_idx in range(num_cols):
        col = cell_factory(col_idx, row_idx, num_cols, num_rows)
        row.append(col)
      matrix.append(row)
    return matrix

  def make_cell_x(self, col, row, num_cols, num_rows):
    return col == row or col + row == (num_cols + num_rows) / 2

  def make_cell_random(self, col, row, num_cols, num_rows):
    return bool(random.getrandbits(1)) # random bool

  def count_neighbors(self, matrix, col, row):
    count = 0
    for neighbor in self.mask:
      neighbor_row = (row + neighbor[0]) % len(matrix)
      neighbor_col = (col + neighbor[1]) % len(matrix[0])
      if matrix[neighbor_row][neighbor_col]:
        count += 1
    return count

  def determine_value(self, old_value, count):
    return old_value and count == 2 or count == 3

  def update(self, step_size, old_state):

    matrix = []
    for row_idx, old_row in enumerate(old_state['matrix']):
      row = []
      for col_idx, old_value in enumerate(old_row):
        count = self.count_neighbors(old_state['matrix'], col_idx, row_idx)
        value = self.determine_value(old_value, count)
        row.append(value)
      matrix.append(row)

    return {'matrix': matrix}

if __name__ == '__main__':
  simulate(GameOfLife())
