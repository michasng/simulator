import math
from simulator.core.util import randnum
from simulator.core.vector import *
from simulator.simulator import simulate, visualize
from simulator.simulation.simulation import Simulation
import numpy as np

def calc_distances(earth, other_bodies, old_distances, step_count):
  min_distance_mag = math.inf
  min_distance = None
  distances = []
  for index, other in enumerate(other_bodies):
    mag = magnitude(earth['pos'] - other['pos'])
    average_mag = (old_distances[index]['average_mag'] * (step_count - 1) + mag) / step_count
    distance = {
      'origin': earth['mapped_pos'],
      'target': other['mapped_pos'],
      'magnitude': mag,
      'average_mag': average_mag,
      'color': 'white',
      'time': old_distances[index]['time']
    }
    if mag < min_distance_mag:
      min_distance_mag = mag
      min_distance = distance
    distances.append(distance)
  min_distance['color'] = 'green'
  min_distance['time'] += 1
  return distances

def get_cam_focus(center, size):
  half_size_vec = vec(size, size) * 0.5
  return {
    'start': center - half_size_vec,
    'end': center + half_size_vec,
  }

# using astronomical units for distance, solar masses for mass and years for time
class PlanetsDistances(Simulation):

  def init(self, params):
    self.gravitational_constant = params['gravitational_constant']
    self.space_size = params['space_size']

    bodies = params['initial_bodies']
    # auto-generate id fields
    for i, body in enumerate(bodies):
      body['id'] = i
      body['pos'] = vec(*body['pos'])
      body['vel'] = vec(*body['vel'])

    return [{
      'bodies': bodies,
    }]

  def move_bodies(self, step_size, old_bodies):
    bodies = []

    # attraction
    for entity in old_bodies:
      total_force = vec(0, 0)
      for other in old_bodies:
        if entity == other:
          continue
        distance = other['pos'] - entity['pos']
        distance_mag = magnitude(distance)
        distance_mag_squared = distance_mag * distance_mag
        direction = normalize(distance)
        force = direction * self.gravitational_constant * entity['mass'] * other['mass'] / distance_mag_squared
        total_force += force

      acceleration = total_force / entity['mass']
      # consider timestep size
      vel = entity['vel'] + acceleration * step_size
      
      pos = entity['pos'] + vel * step_size
      bodies.append({
        'id': entity['id'],
        'mass' : entity['mass'],
        'radius' : entity['radius'],
        'pos' : pos,
        'vel' : vel,
      })
    return bodies

  def update(self, step_size, old_state):
    bodies = self.move_bodies(step_size, old_state['bodies'])
    return {
      'bodies': bodies,
    }

if __name__ == '__main__':
  simulate(PlanetsDistances())
