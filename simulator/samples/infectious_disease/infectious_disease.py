import random, math, time
import numpy as np
from simulator.core.util import randnum
from simulator.core.vector import *
from simulator.simulator import simulate
from simulator.simulation.simulation import Simulation


class CollisionGrid():
  def __init__(self, num_cols, num_rows, width, height):
    self.num_cols = num_cols
    self.num_rows = num_rows
    self.width = width
    self.height = height
    self.cell_width = width / num_cols
    self.cell_height = height / num_rows
    
    self.cells = [[[] for cell in range(num_cols)] for row in range(num_rows)]

  def to_grid_pos(self, pos):
    return [int(pos[0] / self.cell_width), int(pos[1] / self.cell_height)]

  def add(self, value, pos):
    grid_pos = self.to_grid_pos(pos)
    self.get_cell_in_bounds(grid_pos).append(value)
  
  def remove(self, value, pos):
    grid_pos = self.to_grid_pos(pos)
    self.get_cell_in_bounds(grid_pos).remove(value)

  def get_neighbors(self, pos, value):
    grid_pos = self.to_grid_pos(pos)
    
    neighbors = []
    for row in range(max(0, grid_pos[1] - 1), min(self.num_rows, grid_pos[1] + 2)):
      for col in range(max(0, grid_pos[0] - 1), min(self.num_cols, grid_pos[0] + 2)):
        neighbors += self.cells[row][col]
    neighbors.remove(value)
    return neighbors

  def get_cell_in_bounds(self, grid_pos):
    return self.cells[grid_pos[1]][grid_pos[0]]
  
  def get_cell(self, grid_pos):
    if grid_pos[0] < 0 or grid_pos[0] >= self.num_cols or grid_pos[1] < 0 or grid_pos[1] >= self.num_rows:
      raise ValueError('Out of Bounds.')
    return self.get_cell_in_bounds(grid_pos)

R = 0 # recovered
S = 1 # susceptible
I = 2 # infected
V = 3 # vaccinated
NUM_STATES = 4

class InfectiousDisease(Simulation):

  def init(self, params):
    self.width = params['width']
    self.height = params['height']
    self.velocity = params['velocity']
    self.infection_radius = params['infection_radius']
    self.infection_chance = params['infection_chance']
    self.infection_duration = params['infection_duration']
    # every person might be replaced by a susceptible one
    self.replace_chance = params['replace_chance']
    # infected might go into quarantine
    self.quarantine_delay = params['quarantine_delay']
    self.quarantine_chance = params['quarantine_chance']
    self.quarantine_duration = params['quarantine_duration']
    # replaced people might be vaccinated starting at a certain day, ending at a certain percentage
    self.vaccination_invention = params['vaccination_invention']
    self.vaccination_chance = params['vaccination_chance']
    self.vaccination_stop = params['vaccination_stop']
    self.update_count = 0

    self.grid = CollisionGrid(params['num_cols'], params['num_rows'], self.width, self.height)

    seed = params['seed'] if 'seed' in params else int(time.time())
    self.rd_populate = random.Random(seed)
    self.rd_infect = random.Random(seed)
    self.rd_replace = random.Random(seed)
    self.rd_vaccine = random.Random(seed)
    self.rd_quarantine = random.Random(seed)

    population = []
    for index, comp_param in enumerate(params['compartments']):
      sub_population = self.make_population(index, comp_param['init'])
      population += sub_population
    for index, person in enumerate(population):
      self.grid.add(index, person['pos'])
    comp_counts = [comp['init'] for comp in params['compartments']]

    population_density = (len(population) - 1) / (self.width * self.height)
    infection_area = self.infection_radius * self.infection_radius * math.pi
    contact_rate = population_density * infection_area
    params['r0'] = self.infection_chance * self.infection_duration * contact_rate
    
    return [{
      'population': population,
      'comp_counts': comp_counts,
      'comp_change_counts': comp_counts,
      'last_day_i': -1,
      'r': 0,
      'r0': 0,
    }]
  
  def make_population(self, state, num):
    n = []
    for i in range(num):
      n.append(self.make_person(state))
    return n

  def make_person(self, state):
    # random point on a square plane
    pos = vec(randnum(0, self.width, self.rd_populate), randnum(0, self.height, self.rd_populate))
    # random direction with fixed magnitude (random point on a ring)
    vel = randunitvec(self.rd_populate) * self.velocity
    comp = self.make_comp(state)
    comp['quarantined'] = False
    return {
      'pos': pos,
      'vel': vel,
      'comp': comp,
    }
  
  def make_comp(self, state):
    return {
      'state': state,
      'last_change': self.update_count,
    }

  def move(self, old_pos, old_vel):
    def bounce(index, size, pos, vel):
      if pos[index] < 0:
        pos[index] = - pos[index]
        vel[index] *= -1
      if pos[index] >= size:
        pos[index] = (size - 1) - (pos[index] - size)
        vel[index] *= -1

    vel = np.copy(old_vel)
    pos = old_pos + vel
    bounce(0, self.width, pos, vel)
    bounce(1, self.height, pos, vel)
    return pos, vel

  def find_neighbors(self, population, index, state):
    pos = population[index]['pos']
    grid_neighbors = self.grid.get_neighbors(pos, index)
    neighbors = []
    for other_idx in grid_neighbors:
      # ignore quarantined people, or those in the wrong state
      if population[other_idx]['comp']['quarantined'] or \
        population[other_idx]['comp']['state'] != state:
        continue
      distance = magnitude(pos - population[other_idx]['pos'])
      if distance <= self.infection_radius:
        neighbors.append(other_idx)
    return neighbors

  def get_updated_compartment(self, old_state, index):
    old_comp = old_state['population'][index]['comp']
    # amount of steps that the compartment has not changed
    no_comp_change_since = self.update_count - old_comp['last_change']

    # every removed person might be replaced (give birth + die, immunity dies off, etc.)
    if old_comp['state'] == R and self.rd_replace.random() < self.replace_chance:
      return self.make_comp(S)

    # susceptible might become infected or vaccinated
    if old_comp['state'] == S and not old_comp['quarantined']:
      # find neighbors
      infected_neighbors = self.find_neighbors(old_state['population'], index, I)
      # calculate chance to be infected by at least one neighbor
      total_infection_chance = 1 - math.pow(1 - self.infection_chance, len(infected_neighbors))
      if self.rd_infect.random() < total_infection_chance:
        return self.make_comp(I)
      
      # if vaccines have already been invented and
      # a certain ratio of the population is un-vaccinated,
      # there's a chance of being vaccinated
      vaccinated_ratio = (old_state['comp_counts'][V] + self.temp_vaccinated) / len(old_state['population'])
      if self.vaccination_invention != -1 and self.update_count >= self.vaccination_invention and \
        vaccinated_ratio < self.vaccination_stop and \
        self.rd_vaccine.random() < self.vaccination_chance:
        self.temp_vaccinated += 1
        return self.make_comp(V)

    elif old_comp['state'] == I: # infected will be removed after a certain amount of time
      if no_comp_change_since == self.infection_duration:
        return self.make_comp(R)

    return old_comp

  def update_quarantined(self, old_state, index, comp):
    old_comp = old_state['population'][index]['comp']
    no_comp_change_since = self.update_count - comp['last_change']

    if old_comp['quarantined']:
      # check if should leave quarantine, or if the person was replaced
      if self.update_count - old_comp['quarantine_start'] == self.quarantine_duration or \
        no_comp_change_since == 0 and comp['state'] != I:
        comp['quarantined'] = False
      else: # stay in quarantine
        comp['quarantined'] = True
        comp['quarantine_start'] = old_comp['quarantine_start']
    else:
      # check if should enter quarantine
      if comp['state'] == I and \
        no_comp_change_since == self.quarantine_delay and \
        self.rd_quarantine.random() < self.quarantine_chance:
        comp['quarantined'] = True
        comp['quarantine_start'] = self.update_count
      else: # stay out of quarantine
        comp['quarantined'] = False

  def update(self, step_size, old_state):
    self.update_count += 1
    self.temp_vaccinated = 0

    # number of members of each state
    comp_counts = [0 for i in range(NUM_STATES)]
    # number of people new members to each state
    comp_change_counts = [0 for i in range(NUM_STATES)]
    population = []
    for index, old_person in enumerate(old_state['population']):
      # update state
      comp = self.get_updated_compartment(old_state, index)
      self.update_quarantined(old_state, index, comp)

      # count states
      comp_counts[comp['state']] += 1
      if comp['state'] != old_person['comp']['state']:
        comp_change_counts[comp['state']] += 1
      
      # move
      pos, vel = self.move(old_person['pos'], old_person['vel'])
      person = {
        'pos': pos,
        'vel': vel,
        'comp': comp,
      }
      population.append(person)

    # update grid
    for index, person in enumerate(population):
      self.grid.remove(index, old_state['population'][index]['pos'])
      self.grid.add(index, person['pos'])

    # r = the average number of people each infected person infects
    if old_state['comp_counts'][I] == 0:
      r = math.nan
    else:
      # number of new infections for each old infected * expected number of days for this to happen
      r = (comp_change_counts[I] / old_state['comp_counts'][I]) * self.infection_duration
    # r0 = r, but independent of the portion of succeptible people in the population
    if len(old_state['population'])  == 0 or old_state['comp_counts'][S] == 0:
      r0 = math.nan
    r0 = r / (old_state['comp_counts'][S] / len(old_state['population']) )
    last_day_i = old_state['last_day_i'] if comp_counts[I] == 0 else self.update_count
    return {
      'population': population,
      'comp_counts': comp_counts,
      'comp_change_counts': comp_change_counts,
      'last_day_i': last_day_i,
      'r': r,
      'r0': r0,
    }

if __name__ == '__main__':
  simulate(InfectiousDisease())
