from simulator.simulator import simulate
from simulator.simulation.simulation import Simulation

class Skeleton(Simulation):

  def init(self, params):
    return [{}]

  def update(self, step_size, old_state):
    return {}

if __name__ == '__main__':
  simulate(Skeleton())
