import random
import multiprocessing as mp
from simulator.simulator import simulate
from simulator.simulation.simulation import Simulation

def monte_carlo_step():
  # random point with x, y in range [-1, 1]
  x = random.random() * 2 - 1
  y = random.random() * 2 - 1
  within_unit_circle = (x * x + y * y) <= 1
  return {'x': x, 'y': y, 'within_unit_circle': within_unit_circle}

def monte_carlo_pi(in_unit_cirlce, total):
  return (in_unit_cirlce / total) * 4

class MonteCarloParallel():

  def process_function(self, argument_queue, result_queue):
    while True:
      argument_queue.get()
      res = monte_carlo_step()
      result_queue.put(res)
      argument_queue.task_done()

  def init(self, params):
    self.in_unit_cirlce = 0
    self.total = 0

    self.argument_queue = mp.JoinableQueue()
    self.result_queue = mp.Queue()
    nr_of_processes = mp.cpu_count()
    processes = [
      mp.Process(target = self.process_function, args = (self.argument_queue, self.result_queue))
      for i in range(nr_of_processes)
    ]
    for p in processes:
      p.start()
    self.processes = processes

    return None

  # override
  def update_batch(self, step_size, old_state, batch_size):
    new_states = []
    for i in range(batch_size):
      self.argument_queue.put(i)
    self.argument_queue.join() # locks until all tasks are done
    while not self.result_queue.empty():
      res = self.result_queue.get()

      if res['within_unit_circle']:
        self.in_unit_cirlce += 1
      self.total += 1
      res['pi_approx'] = monte_carlo_pi(self.in_unit_cirlce, self.total)

      new_states.append(res)
    return new_states

  # override
  def quit(self):
    for p in self.processes:
      p.terminate()

if __name__ == '__main__':
  simulate(MonteCarloParallel())
