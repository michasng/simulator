from simulator.simulator import simulate
from simulator.simulation.simulation import Simulation
import random

def monte_carlo_step():
  # random point with x, y in range [-1, 1]
  x = random.random() * 2 - 1
  y = random.random() * 2 - 1
  within_unit_circle = (x * x + y * y) <= 1
  return {'x': x, 'y': y, 'within_unit_circle': within_unit_circle}

def monte_carlo_pi(in_unit_cirlce, total):
  return (in_unit_cirlce / total) * 4

class MonteCarlo(Simulation):
  def init(self, params):
    self.in_unit_cirlce = 0
    self.total = 0
    return None

  def update(self, step_size, old_state):
    res = monte_carlo_step()

    if res['within_unit_circle']:
      self.in_unit_cirlce += 1
    self.total += 1
    res['pi_approx'] = monte_carlo_pi(self.in_unit_cirlce, self.total)
    return res

if __name__ == '__main__':
  simulate(MonteCarlo())
