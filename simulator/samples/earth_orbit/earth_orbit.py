import math
from simulator.core.util import randnum
from simulator.core.vector import *
from simulator.simulator import simulate, visualize
from simulator.simulation.simulation import Simulation
import numpy as np

# using astronomical units for distance, solar masses for mass and years for time
class EarthOrbit(Simulation):

  def init(self, params):
    self.gravitational_constant = params['gravitational_constant']
    self.space_size = params['space_size']

    bodies = params['bodies']
    # auto-generate id fields
    for i, body in enumerate(bodies):
      body['id'] = i
      body['pos'] = vec(*body['pos'])
      body['vel'] = vec(*body['vel'])

    return [{
      'bodies': bodies,
    }]

  def get_distance_vector(self, p1, p2):
    # in euclidean space:
    euclidian_distance_vector = p2 - p1
    # in non-euclidean space:
    constrained_vector = euclidian_distance_vector % self.space_size
    if constrained_vector[0] > self.space_size / 2:
      constrained_vector[0] = constrained_vector[0] - self.space_size
    if constrained_vector[1] > self.space_size / 2:
      constrained_vector[1] = constrained_vector[1] - self.space_size
    return constrained_vector

  def update(self, step_size, old_state):
    bodies = []

    # attraction
    for entity in old_state['bodies']:
      total_force = vec(0, 0)
      for other in old_state['bodies']:
        if entity == other:
          continue
        distance = self.get_distance_vector(entity['pos'], other['pos'])
        distance_mag = magnitude(distance)
        distance_mag_squared = distance_mag * distance_mag
        direction = normalize(distance)
        force = direction * self.gravitational_constant * entity['mass'] * other['mass'] / distance_mag_squared
        total_force += force

      acceleration = total_force / entity['mass']
      # consider timestep size
      vel = entity['vel'] + acceleration * step_size
      
      pos = entity['pos'] + vel * step_size
      pos = pos % self.space_size # constrain space
      bodies.append({
        'id': entity['id'],
        'mass' : entity['mass'],
        'radius' : entity['radius'],
        'pos' : pos,
        'vel' : vel,
      })
    return {
      'bodies': bodies,
    }

if __name__ == '__main__':
  simulate(EarthOrbit())
  # visualize()
