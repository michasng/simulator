import random, math
from simulator.core.util import randnum
from simulator.core.vector import randunitvec, vec
from simulator.simulator import simulate
from simulator.simulation.simulation import Simulation

# from https://stackoverflow.com/questions/3838329/how-can-i-check-if-two-segments-intersect
def intersects(s0,s1):
  # assumes line segments are stored in the format [(x0,y0),(x1,y1)]
  dx0 = s0[1][0]-s0[0][0]
  dx1 = s1[1][0]-s1[0][0]
  dy0 = s0[1][1]-s0[0][1]
  dy1 = s1[1][1]-s1[0][1]
  p0 = dy1*(s1[1][0]-s0[0][0]) - dx1*(s1[1][1]-s0[0][1])
  p1 = dy1*(s1[1][0]-s0[1][0]) - dx1*(s1[1][1]-s0[1][1])
  p2 = dy0*(s0[1][0]-s1[0][0]) - dx0*(s0[1][1]-s1[0][1])
  p3 = dy0*(s0[1][0]-s1[1][0]) - dx0*(s0[1][1]-s1[1][1])
  return (p0*p1<=0) & (p2*p3<=0)

# from https://stackoverflow.com/questions/20677795/how-do-i-compute-the-intersection-point-of-two-lines
def line_intersection(line1, line2):
  xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
  ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

  def det(a, b):
    return a[0] * b[1] - a[1] * b[0]

  div = det(xdiff, ydiff)
  if div == 0:
    raise Exception('lines do not intersect')

  d = (det(*line1), det(*line2))
  x = det(d, xdiff) / div
  y = det(d, ydiff) / div
  return x, y

def get_intersection_point(needle, width, ys):
  for y in ys:
    other = [[0, y], [width, y]]
    if intersects(needle, other):
      return line_intersection(needle, other)
  return False

class BuffonNeedle(Simulation):

  def init(self, params):
    self.width = params['width']
    self.height = params['height']
    self.length = params['l'] # distance between lines and length of needles
    self.num_needles = 0
    self.num_points = 0
    return None

  # adds one needle
  def update(self, step_size, old_state):
    res = {}

    # choose a random center point with some space
    radius = self.length / 2
    center = vec(
      randnum(radius, self.width - radius),
      randnum(radius, self.height - radius)
    )
    # random rotation
    offset = randunitvec() * radius
    res['start'] = center + offset
    res['end'] = center - offset
    self.num_needles += 1

    # check if crossing lines
    ys = [i * self.length + self.length for i in range(int(self.width / self.length) - 1)]
    point = get_intersection_point([res['start'], res['end']], self.width, ys)
    if point:
      res['point'] = point
      self.num_points += 1

    if self.num_points == 0:
      res['pi_approx'] = 0
    else:
      res['pi_approx'] = 2 * self.num_needles / self.num_points

    return res

if __name__ == '__main__':
  simulate(BuffonNeedle())
