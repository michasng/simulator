def pie_init(wrapped_data, text_is_percentage):
  total = 0
  for data in wrapped_data:
    total += data['value']
  start_percentage = 0
  for data in wrapped_data:
    data['start_percentage'] = start_percentage
    data['percentage'] = data['value'] / total
    start_percentage += data['percentage']
    data['text_position'] = [20, 20 + 20 * data['index']]
    if text_is_percentage:
      data['text'] = data['percentage']
    else:
      data['text'] = data['value']

def areas_init(datas, max_value, colors, width, height, border_size, limit_values):
  h = height - 2 * border_size
  w = width - 2 * border_size
  factor = h / max_value
  container = []
  # for each area, back to front
  for area_idx, area_data in enumerate(datas):
    def sum_values(value_idx):
      return sum(datas[temp_area_idx][value_idx] for temp_area_idx in range(area_idx, len(datas)))

    wrapped = {
      'color': colors[area_idx],
      'points': [[w + border_size, border_size], [border_size, border_size]]
    }
    if len(area_data) == 1: # flat, linear area
      value = sum_values(0) * factor
      wrapped['points'].append([border_size, value + border_size])
      wrapped['points'].append([w + border_size, value + border_size])
    else:
      # optimize by only showing certain steps, causes artifacts
      step = len(area_data) // limit_values
      step = max(step, 1) # step must not be 0
      num_segments = len(area_data) / step

      segment_width = w / (num_segments - 1 / step)
      # for each value
      for value_idx in range(0, len(area_data), int(step)):
        # if area_data[value_idx] == 0:
        # value = 0 # optimize for hidden areas, causes minor artifacts
        # sum with the values from areas in front of this one
        value = sum_values(value_idx) * factor
        x = (value_idx / step) * segment_width
        wrapped['points'].append([x + border_size, value + border_size])
      last_idx = len(area_data) - 1
      # make sure the last index is included, even if it was skipped
      if value_idx < last_idx:
        value = sum_values(last_idx) * factor
        x = (last_idx / step) * segment_width
        wrapped['points'].append([x + border_size, value + border_size])

    container.append(wrapped)

  return container
