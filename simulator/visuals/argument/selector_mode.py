import math, statistics
from simulator.core.util import filter_nan

class SelectorMode:

  ALL = 'all'
  NNAN = 'nnan'
  FIRST = 'first'
  LAST = 'last'
  LEN = 'len'
  SUM = 'sum'
  MIN = 'min'
  MAX = 'max'
  RANGE = 'range'
  MEAN = 'mean'
  MEDIAN = 'median'
  MODE = 'mode'
  P_VARIANCE = 'p_variance'
  P_STDEV = 'p_stdev'
  S_VARIANCE = 's_variance'
  S_STDEV = 's_stdev'

  def get(self, contexts, as_value):
    raise NotImplementedError

  @classmethod
  def from_arg(cls, arg):
    if arg == SelectorMode.ALL:
      return AllSelectorMode()
    if arg == SelectorMode.NNAN:
      return NnanSelectorMode()
    if arg == SelectorMode.FIRST:
      return FirstSelectorMode()
    if arg == SelectorMode.LAST:
      return LastSelectorMode()
    if arg == SelectorMode.LEN:
      return LenSelectorMode()
    if arg == SelectorMode.SUM:
      return SumSelectorMode()
    if arg == SelectorMode.MIN:
      return MinSelectorMode()
    if arg == SelectorMode.MAX:
      return MaxSelectorMode()
    if arg == SelectorMode.RANGE:
      return RangeSelectorMode()
    if arg == SelectorMode.MEAN:
      return MeanSelectorMode()
    if arg == SelectorMode.MEDIAN:
      return MedianSelectorMode()
    if arg == SelectorMode.MODE:
      return ModeSelectorMode()
    if arg == SelectorMode.P_VARIANCE:
      return PVarianceSelectorMode()
    if arg == SelectorMode.P_STDEV:
      return PStDevSelectorMode()
    if arg == SelectorMode.S_VARIANCE:
      return SVarianceSelectorMode()
    if arg == SelectorMode.S_STDEV:
      return SStDevSelectorMode()
    raise ValueError(f'SelectorMode not defined: {arg}.')

  @classmethod
  def partition_selector(cls, arg, default_mode):
    (l, p, r) = arg.partition('#')
    
    if not p: # list empty
      return (arg, SelectorMode.from_arg(default_mode))
    
    str_mode = r.strip().lower()
    mode = SelectorMode.from_arg(str_mode)
    return (l, mode)

class AllSelectorMode(SelectorMode):

  def get(self, contexts, as_value):
    if as_value:
      return [c.value for c in contexts]
    return contexts

class NnanSelectorMode(SelectorMode):
  def get(self, contexts, as_value):
    if as_value:
      return [c.value for c in contexts if not math.isnan(c.value)]
    return [c for c in contexts if not math.isnan(c.value)]

# will only ever return a single value, never a context
class SingleValueSelectorMode(SelectorMode):

  def get(self, contexts, as_value):
    if not contexts:
      raise ValueError('SingleValueSelector requires at least one value.')
    if not as_value:
      raise ValueError('ValueSelector cannot select contexts, just values.')
    values = [c.value for c in contexts]
    return self.get_one(values)

  def get_one(self, values):
    raise NotImplementedError()

class SingleSelectorMode(SelectorMode):
  
  def get(self, contexts, as_value):
    if not contexts:
      raise ValueError('SingleSelector requires at least one value.')
    context = self.get_one(contexts)
    if as_value:
      return context.value
    return context
  
  def get_one(self, contexts):
    raise NotImplementedError()

class FirstSelectorMode(SingleSelectorMode):

  def get_one(self, contexts):
    return contexts[0]
    
class LastSelectorMode(SingleSelectorMode):

  def get_one(self, contexts):
    return contexts[-1]

class LenSelectorMode(SingleValueSelectorMode):
  
  def get_one(self, values):
    return len(values)

class SumSelectorMode(SingleValueSelectorMode):
  
  def get_one(self, values):
    filtered = filter_nan(values)
    return sum(filtered)

class MinSelectorMode(SingleSelectorMode):

  def get_one(self, contexts):
    return min(contexts, key = lambda k: k.value)

class MaxSelectorMode(SingleSelectorMode):

  def get_one(self, contexts):
    return max(contexts, key = lambda k: k.value)

class RangeSelectorMode(SingleValueSelectorMode):

  def get_one(self, values):
    return max(values) - min(values)

class MeanSelectorMode(SingleValueSelectorMode):

  # arithmetic mean (average)
  def get_one(self, values):
    filtered = filter_nan(values)
    if not filtered:
      return math.nan
    return statistics.mean(filtered)

class MedianSelectorMode(SingleValueSelectorMode):

  # mean of middle two method
  def get_one(self, values):
    return statistics.median(filter_nan(values))

class ModeSelectorMode(SingleValueSelectorMode):

  # first of the most common data points
  def get_one(self, values):
    return statistics.mode(values)

class PVarianceSelectorMode(SingleValueSelectorMode):
  
  def get_one(self, values):
    filtered = filter_nan(values)
    if not filtered:
      return 0
    return statistics.pvariance(filtered)

class PStDevSelectorMode(SingleValueSelectorMode):
  
  def get_one(self, values):
    filtered = filter_nan(values)
    if not filtered:
      return 0
    return statistics.pstdev(filtered)

class SVarianceSelectorMode(SingleValueSelectorMode):
  
  def get_one(self, values):
    filtered = filter_nan(values)
    if not filtered or len(values) < 2:
      return 0
    return statistics.variance(filtered)
    
class SStDevSelectorMode(SingleValueSelectorMode):
  
  def get_one(self, values):
    filtered = filter_nan(values)
    if not filtered or len(values) < 2:
      return 0
    return statistics.stdev(filtered)
