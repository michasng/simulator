from jsonpath_ng.ext import parse
from simulator.core.vector import vec
from simulator.visuals.color import parse_color
from simulator.visuals.argument.selector_mode import SelectorMode

class Argument:
  def get(self, context, as_value):
    raise NotImplementedError

  @classmethod
  def from_arg(cls, arg, is_color = False, default_mode = SelectorMode.FIRST):
    if type(arg) is not str:
      # Color-ValueArguments are just ArrayArguments containing ValueArguments
      if type(arg) is list:
        return ArrayArgument(arg)
      if type(arg) is dict:
        return DictArgument(arg)
      return ValueArgument(arg)

    # if the string is surrounded by single-quotes, then it's not a selector
    if len(arg) > 1 and arg[0] == '\'' and arg[-1] == '\'':
      arg = arg[1:-1]
      if is_color:
        return ValueArgument(parse_color(arg))
      return ValueArgument(arg)
 
    if is_color:
      return ColorSelectorArgument(arg, default_mode)
    return SelectorArgument(arg, default_mode)

class ValueArgument(Argument):
  def __init__(self, arg):
    self.value = arg

  def get(self, context, as_value):
    return self.value

class ArrayArgument(Argument):
  def __init__(self, args):
    self.args = [Argument.from_arg(arg) for arg in args]
  
  def get(self, context, as_value):
    res = [arg.get(context, as_value) for arg in self.args]
    if as_value:
      return vec(*res)
    return res

class DictArgument(Argument):
  def __init__(self, dictionary):
    self.args = {}
    for key in dictionary:
      self.args[key] = Argument.from_arg(dictionary[key])

  def get(self, context, as_value):
    res = {}
    for key in self.args:
      res[key] = self.args[key].get(context, as_value)
    return res

class SelectorArgument(Argument):
  def __init__(self, arg, default_mode):
    self.arg = arg
    selector_arg, self.mode = SelectorMode.partition_selector(arg, default_mode)
    try:
      self.selector = parse(selector_arg)
    except Exception:
      raise ValueError(f'Could not parse SelectorArgument "{selector_arg}".')

  def get(self, context, as_value):
    res = self.selector.find(context) # fails silently
    try: # location of try/except is correct, mode can raise ValueError
      res = self.mode.get(res, as_value)
    except ValueError:
      raise ValueError(f'SelectorArgument "{self.arg}" could not find any values in "{context}".')
    return res

class ColorSelectorArgument(SelectorArgument):
  # override
  def get(self, context, as_value):
    return parse_color(super().get(context, as_value))
