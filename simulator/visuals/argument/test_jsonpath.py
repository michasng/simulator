import timeit

setup = '''
from jsonpath_ng.ext import parse

circle = {
  'nested': {
    'color': '#ff0000',
  },
  'position': [100, 100],
  'radius': 10
}

args = [
  'nested.color',
  'position',
  '@.radius * 2',
]
'''

parse = '''
parsed = [parse(arg) for arg in args]
'''

select = '''
results = [selector.find(circle) for selector in parsed]
'''

print_res = '''
print([[r.value for r in res] for res in results])
'''

if __name__ == '__main__':

  print(timeit.timeit(stmt=parse, setup=setup, number=100))
  # 6.0084295
  print(timeit.timeit(stmt=select, setup=setup+parse, number=100))
  # 0.001451499999999939

