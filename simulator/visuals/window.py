import os, tempfile, cv2
from shutil import copyfile
from pyglet import window, app, clock, gl, image
from simulator.config import get_app_config
from simulator.visuals.loop import CappedLoop, UncappedLoop
from simulator.visuals.color import parse_color, NAMED_COLORS

class Window(window.Window):

  def __init__(self, visualizer, config):
    self.visualizer = visualizer
    super().__init__(
      width = config['size'][0],
      height = config['size'][1],
      visible = True,
      vsync = False, # has to be disabled manually
    )
    self.__config = config # name '_config' is already used by window.Window

    # setting visible=False in constructor would prevent shapes from rendering on screenshots    
    if not config['visibility']:
      self.set_visible(False)
    
    if config['record']:
      self.temp_dir = tempfile.TemporaryDirectory()
      self.screenshots_taken = 0

    bg_color = parse_color(
      config['background_color'],
      as_float = True,
      include_alpha = True,
    )
    gl.glClearColor(*bg_color)

    self.show_fps = config['show_fps']
    if self.show_fps:
      self.fps_display = window.FPSDisplay(window = self)
      self.fps_display.label.x = config['border_size']
      self.fps_display.label.y = config['border_size']
      # self.fps_display.label.color = (0, 0, 0, 255)
    self.frame_count = 0

  # override
  def on_key_press(self, symbol, modifiers):
    self.visualizer.on_key_press(symbol, modifiers)

  # override
  def on_draw(self):
    self.render()
    self.frame_count += 1
    
    if self.__config['record']:
      self.save_image(self.temp_dir.name, self.current_screenshot_name())
      self.screenshots_taken += 1

  def current_screenshot_name(self, offset=0):
    img_name = str(self.screenshots_taken + offset).zfill(10) # x digits, pad with leading 0s
    return f'{img_name}.png'

  # override
  def on_close(self):
    super().on_close()
    if self.__config['record']:
      if self.__config['keep_last_frame']:
        src = os.path.join(self.temp_dir.name, self.current_screenshot_name(-1))
        copyfile(src, 'last_frame.png')
      self.save_video(self.temp_dir.name, 'video.mp4')
      self.temp_dir.cleanup()
    self.visualizer.on_quit()

  def exit(self):
    # schedule closing window after drawing finished
    clock.schedule_once(lambda dt: self.on_close(), 0)

  def render(self):
    self.clear()
    self.visualizer.render()
    if self.show_fps:
      self.fps_display.draw()

  def get_fps(self):
    return self.__config['steps_per_second'] * self.__config['frames_per_step']

  def save_image(self, image_dir, image_file):
    file_path = os.path.join(image_dir, image_file)
    image.get_buffer_manager().get_color_buffer().save(file_path)

  def save_video(self, image_dir, video_file):
    debug = get_app_config()['debug']
    if debug: print('Saving video...')
    images = os.listdir(image_dir)
    images.sort() # sort alphabetically

    if not images:
      print('Cannot save empty video, missing frames.')
      return

    frame = cv2.imread(os.path.join(image_dir, images[0]))
    height, width, layers = frame.shape

    fourcc = cv2.VideoWriter_fourcc(*'mp4v') # define codec
    video = cv2.VideoWriter(video_file, fourcc, self.get_fps(), (width, height))

    for image in images:
      video.write(cv2.imread(os.path.join(image_dir, image)))

    cv2.destroyAllWindows()
    video.release()
    if debug: print(f'Video saved {video_file}.')

  def run(self):
    if self.__config['visibility']:
      app.event_loop = CappedLoop(self.get_fps())
    else:
      app.event_loop = UncappedLoop()
    app.run() # locks the thread
