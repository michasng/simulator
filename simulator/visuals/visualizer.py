from simulator.config import get_app_config
from simulator.visuals.window import Window
from simulator.visuals.plot.plot import Plot
from simulator.visuals.layout import apply_layout
from simulator.visuals.function.function_group import FunctionGroup
from pyglet.window import key
from jsonpath_ng.jsonpath import Child, Fields, Slice

class Visualizer():

  def __init__(self, config, state_fetchable, fetch_state, on_quit):
    self.config = config
    self.state_fetchable = state_fetchable
    self.fetch_state = fetch_state
    self.on_quit = on_quit

    self.create_function_groups()
    size = self.create_plots()
    self.init_window(size)

    # fetch the initial state
    self.current_state_index = -1
    self.state_history = []
    self.next()
    # initialize all prefabs & shape_groups with their initial state (if any)
    self.init_states()
    self.is_running = True

  def create_function_groups(self):
    self.function_groups = [FunctionGroup(function_config) for function_config in self.config['functions']]

  def create_plots(self):
    self.plots = [Plot(plot_config) for plot_config in self.config['plots']]
    return apply_layout(self.plots, self.config['border_size'])

  def init_window(self, size):
    self.window = Window(self, {
      'size': size,
      'background_color': self.config['background_color'],
      'border_size': self.config['border_size'],
      'record': self.config['record'],
      'visibility': self.config['visibility'],
      'keep_last_frame': self.config['keep_last_frame'],
      'steps_per_second': self.config['steps_per_second'],
      'frames_per_step': self.config['frames_per_step'],
      'show_fps': self.config['show_fps'],
    })

  # returns the first state,
  # but wrapped in a context that contains params and all other states
  def top_level_select(self, states):
    full_context = {
      'states': states,
      'config': get_app_config()
    }
    selector = Child(Fields('states'), Slice(-1))
    return selector.find(full_context)[0]

  def init_states(self):
    for i in range(len(self.state_history)):
      context = self.top_level_select(self.state_history[0: i + 1])
      self.run_function_groups(context)
      for plot in self.plots:
        plot.init_state(context)

  def run_function_groups(self, context):
    for function_group in self.function_groups:
      function_group.execute(context)

  def on_key_press(self, symbol, modifiers):
    if symbol == key.SPACE:
      self.pause_play()
    if symbol == key.RIGHT:
      self.next()
    if symbol == key.LEFT:
      self.prev()
  
  def prev(self):
    self.set_state_index(self.current_state_index - 1)

  def next(self):
    state_index = self.current_state_index + 1
    if state_index == len(self.state_history):
      if self.state_fetchable():
        self.state_history.append(self.fetch_state())
      else: # simulation is done
        self.is_running = False
        if self.config['auto_close'] or not self.config['visibility']:
          self.window.exit()

    self.set_state_index(state_index)

  def set_state_index(self, state_index):
    if state_index < 0 or state_index >= len(self.state_history):
      return # invalid state index requested
    
    self.current_state_index = state_index
    self.interpolated_frames = 0 # reset interpolation
    # print(self.current_state_index)

  # all states up to (including) the current_state_index
  def get_current_states(self):
    return self.state_history[:self.current_state_index + 1]

  def pause_play(self):
    self.is_running = not self.is_running

  def render(self):
    if not self.state_history: # e.g. when in simulation: init returns [] and total_steps = 0
      return

    if self.is_running:
      if self.window.frame_count % self.config['frames_per_step'] == 0:
        self.next()

    # plots are offset by their layout
    if self.interpolated_frames == 0:
      context = self.top_level_select(self.get_current_states())
      self.run_function_groups(context)
      for plot in self.plots:
        plot.draw_keyframe(context)
    else:
      progress = self.interpolated_frames / self.config['frames_per_step']
      progress = min(progress, 1) # progress > 1 when paused, staying on one step for long
      for plot in self.plots:
        plot.draw_interpolated(progress)
    
    self.interpolated_frames += 1

  def run(self):
    debug = get_app_config()['debug']
    if debug: print('start visuals')
    # locks the thread
    self.window.run()
    if debug: print('stop visuals')
