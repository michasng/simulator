import math, statistics
from random import randint
from simulator.core.util import *
from simulator.core.vector import *
from simulator.visuals.color import *
from simulator.samples.prefabs.prefab_functions import *

function_map = {
  # arithmetic
  'add': lambda x, y: x + y,
  'sub': lambda x, y: x - y,
  'mul': lambda x, y: x * y,
  'div': lambda x, y: x / y,
  'mod': lambda x, y: x % y,
  'map': map,
  'linear_interpolate': linear_interpolate,
  'random_int': randint,
  # statistics
  'filter_nan': filter_nan,
  'min': min,
  'max': max,
  'mean': statistics.mean, # arithmetic mean (average)
  'median': statistics.median, # mean of middle two method
  'mode': statistics.mode, # first of the most common data points
  'spread': lambda data : max(data) - min(data),
  'percentile': percentile,
  'p_variance': statistics.pvariance,
  'p_stdev': statistics.pstdev,
  's_variance': statistics.variance,
  's_stdev': statistics.stdev,
  # vector
  'vector': vec,
  'random_vector': randvec,
  'dot': dot,
  'magnitude': magnitude,
  'normalize': normalize,
  # color
  'parse_color': parse_color,
  'hsv_to_rgb': hsv_to_rgb,
  'random_color': randcol,
  'index_to_color': index_to_color,
  'different_colors': different_colors,
  # miscellaneous
  'print': print,
  'set': lambda *args: args,
  'list': lambda *args: list(args),
  'range': lambda *args: list(range(*args)),
  'feed_forward': feed_forward,
  'feed_forward_index': feed_forward_index,
  'feed_forward_if': feed_forward_if,
  'wrap_with_index': wrap_with_index,
  'add_index_field': add_index_field,
  'unroll_matrix': unroll_matrix,
  'set_default_values': set_default_values,
  'copy_arg_in_list': copy_arg_in_list,
  # prefab-specific
  'pie_init': pie_init,
  'areas_init': areas_init,
}
