import os, importlib.util
from simulator.core.import_util import import_property
from simulator.config import get_app_config
from simulator.visuals.argument.argument import Argument
from simulator.visuals.argument.selector_mode import SelectorMode
from simulator.visuals.function.executable_functions import function_map

class FunctionGroup:
  
  def __init__(self, config):
    self.config = config
    self.selector = Argument.from_arg(config['context'], default_mode = SelectorMode.ALL)
    self.args = [Argument.from_arg(arg) for arg in config['args']]

    if config['module_name'] is None:
      # pre-defined function
      self.function = function_map[config['function_name']]
      if get_app_config()['debug']:
        print(f'Function loaded (library): {config["function_name"]}.')
    else:
      # custom function
      self.function = import_property(config['module_name'], config['function_name'])
      if get_app_config()['debug']:
        print(f'Function loaded (project): {config["function_name"]}.')

    if self.function is None:
      raise ValueError(f'Executable function not found: {config["function_name"]}.')

  def execute(self, outer_context):
    selected_contexts = self.selector.get(outer_context, as_value = False)
    if type(selected_contexts) is dict: selected_contexts = [selected_contexts]

    # print(f'execute function: {self.config['function_name']} for {len(function_contexts)} contexts')
    for function_context in selected_contexts:
      args = [arg.get(function_context, as_value = True) for arg in self.args]
      ret = self.function(*args)
      # warning: only works for paths, not for dictionary contexts
      if ret is not None and self.config['return_attribute'] is not None:
        function_context.value[self.config['return_attribute']] = ret
        # will only update if the value already exists:
        # parse(self.config['return_attribute']).update(function_context, ret)

