from pyglet import app

class UncappedLoop(app.EventLoop):
  def idle(self):    
    dt = self.clock.update_time()
    self.clock.call_scheduled_functions(dt)

    # Redraw all windows
    for window in app.windows:
        window.switch_to()
        window.dispatch_event('on_draw')
        window.flip()
        window._legacy_invalid = False

    # no timout (sleep-time between idle()-calls)
    return 0


class CappedLoop(app.EventLoop):
  def __init__(self, frames_per_second = 60):
    super().__init__()
    self.frames_per_second = frames_per_second
    self.render_delta_time = 0

  def idle(self):    
    dt = self.clock.update_time()
    self.clock.call_scheduled_functions(dt)

    self.render_delta_time += dt * self.frames_per_second
    if self.render_delta_time >= 1:
      self.render_delta_time -= 1

      # Redraw all windows
      for window in app.windows:
          window.switch_to()
          window.dispatch_event('on_draw')
          window.flip()
          window._legacy_invalid = False

    # no timout (sleep-time between idle()-calls)
    return 0