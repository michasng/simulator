from simulator.core.util import randnum

NAMED_COLORS = {
  'white':  (255, 255, 255),
  'gray':   (105, 115, 126),
  'grey':   (105, 115, 126),
  'lightgray': (210, 210, 210),
  'lightgrey': (210, 210, 210),
  'black':  (0, 0, 0),
  'brown':  (118, 65, 43),
  'red':    (227, 54, 60),
  'orange': (238, 150, 51),
  'yellow': (249, 200, 46),
  'green':  (88, 175, 58),
  'blue':   (14, 107, 168),
  'pink':   (235, 95, 163),
  'purple': (172, 12, 240),
}

def parse_color(color_str, as_float = False, include_alpha = False, alpha = 255):
    if type(color_str) is list:
      color = [int(c) for c in color_str]
    elif color_str in NAMED_COLORS:
      color = NAMED_COLORS[color_str]
    elif len(color_str) == 7 and color_str[0] == '#':
      color = tuple(int(color_str[i:i+2], 16) for i in (1, 3, 5))
    else:
      raise ValueError(f'{color_str} is not a color')
    
    if include_alpha:
      color = (*color, alpha)
    
    if as_float:
      color = (v / 255 for v in color)

    return color

'''
Convert color from hsv to rgb.

@parameter h - hue in range [0, 360].
@parameter s - saturation in range [0, 1].
@parameter v - value in range [0, 1].
@parameter rgb_multiplier - multiplier that is applied to the rgb value.
@parameter alpha - if this is not -1, it is appended to the final result.

@return - rgb value of the color in range [0, rgb_multiplier]
'''
def hsv_to_rgb(h, s, v, rgb_multiplier = 255, alpha = -1):
  c = v * s
  h_ = h / 60
  x = c * (1 - abs(h_ % 2 - 1))
  if 0 <= h_ and h_ < 1:
    rgb_ = [c, x, 0]
  elif h_ < 2:
    rgb_ = [x, c, 0]
  elif h_ < 3:
    rgb_ = [0, c, x]
  elif h_ < 4:
    rgb_ = [0, x, c]
  elif h_ < 5:
    rgb_ = [x, 0, c]
  elif h_ < 6:
    rgb_ = [c, 0, x]
  m = v - c
  rgb = [(i + m) * rgb_multiplier for i in rgb_]
  if alpha != -1:
    rgb.append(alpha)
  return rgb

def randcol(saturation = 1, value = 1):
  hue = randnum(0, 360)
  return hsv_to_rgb(hue, saturation, value)

def index_to_color(index, amount, saturation = 1, value = 1):
  hue = 360 / amount * index
  hue = hue % 360 # in case index > amount
  return hsv_to_rgb(hue, saturation, value)

def different_colors(amount, saturation = 1, value = 1):
  colors = []
  for i in range(amount):
    colors.append(index_to_color(i, amount, saturation, value))
  return colors
