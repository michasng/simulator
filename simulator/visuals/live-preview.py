import time, os, signal
import multiprocessing as mp
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler
from queue import Queue, Empty

from simulator.simulator import visualize

q = Queue()

def on_created(event):
  q.put(f'{event.src_path} has been created.')

def on_deleted(event):
  q.put(f'{event.src_path} has been deleted.')

def on_modified(event):
  q.put(f'{event.src_path} has been modified.')

def on_moved(event):
  q.put(f'{event.src_path} has been moved to {event.dest_path}.')

def run_visuals():
  try:
    visualize()
  except KeyboardInterrupt:
    pass

if __name__ == '__main__':
  patterns = "*"
  ignore_patterns = ""
  ignore_directories = False
  case_sensitive = True
  my_event_handler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories, case_sensitive)

  my_event_handler.on_created = on_created
  my_event_handler.on_deleted = on_deleted
  my_event_handler.on_modified = on_modified
  my_event_handler.on_moved = on_moved

  path = "."
  go_recursively = True
  my_observer = Observer()
  my_observer.schedule(my_event_handler, path, recursive=go_recursively)
  
  my_observer.start()

  p = None
  try:
    while True:
      p = mp.Process(target = run_visuals)
      p.start()
      while True:
        # busy waiting
        if not q.empty():
          event = q.get_nowait()
          print(event)
          # empty the queue after a timeout to batch events
          time.sleep(0.1)
          while not q.empty():
            q.get_nowait() # not printing batched events
          print('Restarting preview...')
          break # restart the process
      p.kill()
  except KeyboardInterrupt:
    pass
  finally:
    my_observer.stop()
    my_observer.join()
    p.join()

