from pyglet import shapes
from simulator.core.util import linear_interpolate_anything
from simulator.visuals.argument.argument import Argument
from simulator.visuals.argument.selector_mode import SelectorMode

class ShapeGroup:

  def __init__(self, config):
    self.config = config
    self.selector = Argument.from_arg(config['context'], default_mode = SelectorMode.ALL)
    self.position = Argument.from_arg(config['args']['position'])
    self.anchor = Argument.from_arg(config['args']['anchor'])
    self.color = Argument.from_arg(config['args']['color'], is_color = True)
    self.last_instances = []
    self.matches = []

  def select_and_create_instances(self, outer_context):
    selected_contexts = self.selector.get(outer_context, as_value = False)
    if type(selected_contexts) is dict: selected_contexts = [selected_contexts]
    return [self.create_instance(node) for node in selected_contexts]

  def create_instance(self, node):
    instance = {
      'position': self.position.get(node, as_value = True),
      'anchor': self.anchor.get(node, as_value = True),
      'color': self.color.get(node, as_value = True),
    }
    if self.config['interpolate_by_id']:
      instance['id'] = node.value['id']
    return instance
  
  def draw_instance(self, instance, offset, batch):
    pass

  def draw_keyframe(self, context, offset, batch):
    instances = self.select_and_create_instances(context)

    batch_memory = [
      self.draw_instance(instance, offset, batch)
      # the keyframe sets off the interpolation -> draws the last frame
      for instance in self.last_instances
    ]
    
    self.matches = self.match_with_last_instances(instances)
    self.last_instances = instances
    return batch_memory
  
  def draw_interpolated(self, progress, offset, batch):
    interpolated_instances = self.interpolate_matches(self.matches, progress)

    batch_memory = [
      self.draw_instance(instance, offset, batch)
      for instance in interpolated_instances
    ]
    
    return batch_memory

  def interpolate_matches(self, matches, progress):
    interpolated_instances = []
    for pair in matches:
      interpolated_instance = {}
      for key in pair[0].keys():
        if key == 'id':
          continue
        res = linear_interpolate_anything(pair[0][key], pair[1][key], progress)
        interpolated_instance[key] = res
      # color must only contain int values
      interpolated_instance['color'] = [int(f) for f in interpolated_instance['color']]
      interpolated_instances.append(interpolated_instance)
    return interpolated_instances

  def match_with_last_instances(self, instances):
    matches = []
    if self.config['interpolate_by_id']:
      '''
      # unoptimized version
      for inst in instances:
        last_inst = inst
        for temp_inst in self.last_instances:
          if temp_inst['id'] == inst['id']:
            last_inst = temp_inst
        matches.append((last_inst, inst))
      '''
      # sort inplace by id (last_instances is already sorted)
      instances.sort(key=lambda inst: inst['id'])
      min_i = 0
      for inst in instances:
        last_inst = inst
        for i in range(min_i, len(self.last_instances)):
          if self.last_instances[i]['id'] == inst['id']:
            last_inst = self.last_instances[i]
            min_i = i + 1 # start the next search where this one ended
        matches.append((last_inst, inst))
    else:
      # this doesn't interpolate new instances.
      # in contrast: zip() doesn't show new instances, until the second step.
      total_instances = len(instances)
      num_new_instances = total_instances - len(self.last_instances)
      if num_new_instances < 0:
        num_new_instances = 0
      num_updated_instances = total_instances - num_new_instances
      for i in range(num_updated_instances):
        matches.append((self.last_instances[i], instances[i]))
      for i in range(num_updated_instances, total_instances):
        matches.append((instances[i], instances[i])) # ignore last_instances

    return matches
