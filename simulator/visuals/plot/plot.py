from pyglet.graphics import Batch
from pyglet.gl import *
from pyglet import shapes
from simulator.visuals.color import parse_color
from simulator.visuals.plot.prefab import Prefab
from simulator.visuals.plot.shape_groups import from_config as shape_group_from_config

class Plot:
  def __init__(self, config):
    self.config = config
    self.color = parse_color(config['background_color'])
    self.create_prefabs()
    self.create_shape_groups()
    self.offset = [0, 0]
  
  def create_prefabs(self):
    self.prefabs = [Prefab.from_config(prefab_ref_config) for prefab_ref_config in self.config['prefabs']]

  def create_shape_groups(self):
    self.shape_groups = [shape_group_from_config(shape_config) for shape_config in self.config['shapes']]

  def init_state(self, context):
    for prefab in self.prefabs:
      prefab.init_state(context)
    for shape_group in self.shape_groups:
      shape_group.last_instances = shape_group.select_and_create_instances(context)

  # constrains the plot, clips everything beyond its bounds
  def clip_plot(self):
    glEnable(GL_STENCIL_TEST)
    glStencilFunc(GL_ALWAYS, 1, 1)
    glStencilOp(GL_REPLACE, GL_REPLACE, GL_REPLACE)

    shapes.Rectangle(
      x = self.offset[0],
      y = self.offset[1],
      width = self.config['width'],
      height = self.config['height'],
      color = self.color,
    ).draw()

    glEnable(GL_STENCIL_TEST)
    glStencilFunc(GL_EQUAL, 1, 1)
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP)

  def disable_clipping(self):
    glDisable(GL_STENCIL_TEST)

  def draw_keyframe(self, context):
    self.clip_plot()

    batch = Batch()
    # must keep references to this data until batch is drawn
    batch_memory = []
    for prefab in self.prefabs:
      b = prefab.draw_keyframe(context, self.offset, batch)
      batch_memory.append(b)
    for shape_group in self.shape_groups:
      b = shape_group.draw_keyframe(context, self.offset, batch)
      batch_memory.append(b)
    batch.draw()
  
    self.disable_clipping()

  def draw_interpolated(self, progress):
    self.clip_plot()

    batch = Batch()
    # must keep references to this data until batch is drawn
    batch_memory = []
    for prefab in self.prefabs:
      b = prefab.draw_interpolated(progress, self.offset, batch)
      batch_memory.append(b)
    for shape_group in self.shape_groups:
      b = shape_group.draw_interpolated(progress, self.offset, batch)
      batch_memory.append(b)
    batch.draw()
  
    self.disable_clipping()
