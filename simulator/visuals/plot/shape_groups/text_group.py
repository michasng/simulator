from pyglet.text import Label
from simulator.visuals.argument.argument import Argument
from simulator.visuals.plot.shape_group import ShapeGroup

class TextGroup(ShapeGroup):

  def __init__(self, config):
    super().__init__(config)
    self.text = Argument.from_arg(config['args']['text'])
    self.font_size = Argument.from_arg(config['args']['font_size'])
    self.width = Argument.from_arg(config['args']['width'])
    self.height = Argument.from_arg(config['args']['height'])
    self.align = Argument.from_arg(config['args']['align'])
  
  # override
  def create_instance(self, node):
    instance = super().create_instance(node)
    instance['text'] = self.text.get(node, as_value = True)
    instance['font_size'] = self.font_size.get(node, as_value = True)
    instance['width'] = self.width.get(node, as_value = True)
    instance['height'] = self.height.get(node, as_value = True)
    instance['align'] = self.align.get(node, as_value = True)
    return instance
  
  # override
  def draw_instance(self, instance, offset, batch):
    position = instance['position']
    anchor = instance['anchor']
    color = instance['color']
    text = instance['text']
    font_size = instance['font_size']
    width = instance['width']
    height = instance['height']
    align = instance['align']

    if type(text) is not str:
      text = '%g'%(text)

    return Label(
      text,
      x = position[0] + offset[0] - width * anchor[0],
      y = position[1] + offset[1] - height * anchor[1],
      font_size = font_size,
      color = [color[0], color[1], color[2], 255],
      batch = batch,
      width = width,
      height = height,
      align = align, # only applies when width != 0
      multiline = True, # has to be set, for 'width' to have any effect
    )
