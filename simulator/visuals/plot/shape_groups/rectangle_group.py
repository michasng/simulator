from pyglet import shapes
from simulator.visuals.argument.argument import Argument
from simulator.visuals.plot.shape_group import ShapeGroup

class RectangleGroup(ShapeGroup):

  def __init__(self, config):
    super().__init__(config)
    self.width = Argument.from_arg(config['args']['width'])
    self.height = Argument.from_arg(config['args']['height'])
    self.opacity = Argument.from_arg(config['args']['opacity'])

  # override
  def create_instance(self, node):
    instance = super().create_instance(node)
    instance['width'] = self.width.get(node, as_value = True)
    instance['height'] = self.height.get(node, as_value = True)
    instance['opacity'] = self.opacity.get(node, as_value = True)
    return instance
  
  # override
  def draw_instance(self, instance, offset, batch):
    position = instance['position']
    anchor = instance['anchor']
    color = instance['color']
    width = instance['width']
    height = instance['height']
    shape = shapes.Rectangle(
      x = position[0] + offset[0] - width * anchor[0],
      y = position[1] + offset[1] - height * anchor[1],
      width = width,
      height = height,
      color = color,
      batch = batch
    )
    shape.opacity = 255 * instance['opacity']
    return shape
