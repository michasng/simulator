from pyglet import shapes, graphics, gl
import tripy
from simulator.visuals.argument.argument import Argument
from simulator.visuals.plot.shape_group import ShapeGroup

class PolygonGroup(ShapeGroup):

  def __init__(self, config):
    super().__init__(config)
    self.points = Argument.from_arg(config['args']['points'])

  # override
  def create_instance(self, node):
    instance = super().create_instance(node)
    instance['points'] = self.points.get(node, as_value = True)
    return instance
  
  # override
  def draw_instance(self, instance, offset, batch):
    position = instance['position']
    anchor = instance['anchor']
    color = instance['color']
    points = instance['points']

    min_x = float('inf') 
    max_x = float('-inf')
    min_y = float('inf')
    max_y = float('-inf')
    for point in points:
      min_x = point[0] if point[0] < min_x else min_x
      max_x = point[0] if point[0] > max_x else max_x
      min_y = point[1] if point[1] < min_y else min_y
      max_y = point[1] if point[1] > max_y else max_y

    x_offset = position[0] + offset[0] - (max_x - min_x) * anchor[0]
    y_offset = position[1] + offset[1] - (max_y - min_y) * anchor[1]

    # triangulate polygon
    triangles = tripy.earclip(points)
    # flatten list of triangles
    triangles = [item for sublist in triangles for item in sublist]

    # flatten points and add offset
    points_flattened = []
    for point in triangles:
      points_flattened.append(point[0] + x_offset)
      points_flattened.append(point[1] + y_offset)

    num_points = len(triangles) # half the length of points_flattened
    batch.add(
      num_points, # points count
      gl.GL_TRIANGLES,
      None, # no group
      ('v2f', points_flattened), # data points
      ('c3B', list(color) * num_points), # color data
    )
