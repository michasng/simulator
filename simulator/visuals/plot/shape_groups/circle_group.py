from enum import Enum
from pyglet import shapes
from simulator.visuals.argument.argument import Argument
from simulator.visuals.plot.shape_group import ShapeGroup

class SizeMode(Enum):
  RADIUS = 1
  DIAMETER = 2

class CircleGroup(ShapeGroup):

  def __init__(self, config):
    super().__init__(config)
    if 'radius' in config['args']:
      self.size_mode = SizeMode.RADIUS
      self.radius = Argument.from_arg(config['args']['radius'])
    else:
      self.size_mode = SizeMode.DIAMETER
      self.diameter = Argument.from_arg(config['args']['diameter'])
    self.opacity = Argument.from_arg(config['args']['opacity'])
  
  # override
  def create_instance(self, node):
    instance = super().create_instance(node)
    if self.size_mode == SizeMode.RADIUS:
      instance['radius'] = self.radius.get(node, as_value = True)
    else:
      instance['diameter'] = self.diameter.get(node, as_value = True)
    instance['opacity'] = self.opacity.get(node, as_value = True)

    return instance
  
  # override
  def draw_instance(self, instance, offset, batch):
    position = instance['position']
    anchor = instance['anchor']
    color = instance['color']
    if self.size_mode == SizeMode.RADIUS:
      radius = instance['radius']
    else:
      radius = 0.5 * instance['diameter']
    shape = shapes.Circle(
      x = position[0] + offset[0] + radius - 2 * radius * anchor[0],
      y = position[1] + offset[1] + radius - 2 * radius * anchor[1],
      radius = radius,
      color = color,
      batch = batch,
    )
    shape.opacity = 255 * instance['opacity']
    return shape
