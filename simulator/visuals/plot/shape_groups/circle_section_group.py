import math
from enum import Enum
from pyglet import shapes, gl, graphics
from simulator.visuals.argument.argument import Argument
from simulator.visuals.plot.shape_group import ShapeGroup

class SizeMode(Enum):
  RADIUS = 1
  DIAMETER = 2

class CircleSectionGroup(ShapeGroup):

  def __init__(self, config):
    super().__init__(config)
    if 'radius' in config['args']:
      self.size_mode = SizeMode.RADIUS
      self.radius = Argument.from_arg(config['args']['radius'])
    else:
      self.size_mode = SizeMode.DIAMETER
      self.diameter = Argument.from_arg(config['args']['diameter'])
    self.percentage = Argument.from_arg(config['args']['percentage'])
    self.start_percentage = Argument.from_arg(config['args']['start_percentage'])

  # override
  def create_instance(self, node):
    instance = super().create_instance(node)
    if self.size_mode == SizeMode.RADIUS:
      instance['radius'] = self.radius.get(node, as_value = True)
    else:
      instance['diameter'] = self.diameter.get(node, as_value = True)
    instance['percentage'] = self.percentage.get(node, as_value = True)
    instance['start_percentage'] = self.start_percentage.get(node, as_value = True)
    return instance
  
  # override
  def draw_instance(self, instance, offset, batch):
    position = instance['position']
    anchor = instance['anchor']
    color = instance['color']
    if self.size_mode == SizeMode.RADIUS:
      radius = instance['radius']
    else:
      radius = 0.5 * instance['diameter']
    percentage = instance['percentage']
    start_percentage = instance['start_percentage']

    segments = 20
    x = position[0] + offset[0] + radius - 2 * radius * anchor[0]
    y = position[1] + offset[1] + radius - 2 * radius * anchor[1]
    r = radius
    angle_per_segment = 2 * math.pi * percentage / segments
    start_angle = 2 * math.pi * start_percentage
    points = x, y # center point
    for i in range(segments + 1): # + 1 to repeat last point
        n = angle_per_segment * i + start_angle
        points += r * math.cos(n) + x, r * math.sin(n) + y
    num_points = segments + 2 # center + segments + repeat last point 
    graphics.draw( # batch.add() mixes the colors in the center (bug?)
      num_points,
      gl.GL_TRIANGLE_FAN,
      # None, # no group, required for batch.add()
      ('v2f', points),
      ('c3B', list(color) * num_points),
    )
