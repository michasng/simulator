from pyglet import shapes
from simulator.visuals.argument.argument import Argument
from simulator.visuals.plot.shape_group import ShapeGroup

class SquareGroup(ShapeGroup):

  def __init__(self, config):
    super().__init__(config)
    self.size = Argument.from_arg(config['args']['size'])
    self.opacity = Argument.from_arg(config['args']['opacity'])

  # override
  def create_instance(self, node):
    instance = super().create_instance(node)
    instance['size'] = self.size.get(node, as_value = True)
    instance['opacity'] = self.opacity.get(node, as_value = True)
    return instance
  
  # override
  def draw_instance(self, instance, offset, batch):
    position = instance['position']
    anchor = instance['anchor']
    color = instance['color']
    size = instance['size']
    shape = shapes.Rectangle(
      x = position[0] + offset[0] - size * anchor[0],
      y = position[1] + offset[1] - size * anchor[1],
      width = size,
      height = size,
      color = color,
      batch = batch
    )
    shape.opacity = 255 * instance['opacity']
    return shape
