from enum import Enum
from pyglet import shapes
from simulator.visuals.argument.argument import Argument
from simulator.visuals.plot.shape_group import ShapeGroup

class SizeMode(Enum):
  TARGET = 1
  VECTOR = 2

class LineGroup(ShapeGroup):

  def __init__(self, config):
    super().__init__(config)
    if 'target' in config['args']:
      self.size_mode = SizeMode.TARGET
      self.target = Argument.from_arg(config['args']['target'])
    else:
      self.size_mode = SizeMode.VECTOR
      self.vector = Argument.from_arg(config['args']['vector'])
    self.width = Argument.from_arg(config['args']['width'])
    self.opacity = Argument.from_arg(config['args']['opacity'])

  # override
  def create_instance(self, node):
    instance = super().create_instance(node)
    if self.size_mode == SizeMode.TARGET:
      instance['target'] = self.target.get(node, as_value = True)
    else:
      instance['vector'] = self.vector.get(node, as_value = True)
    instance['width'] = self.width.get(node, as_value = True)
    instance['opacity'] = self.opacity.get(node, as_value = True)
    return instance
  
  # override
  def draw_instance(self, instance, offset, batch):
    position = instance['position']
    anchor = instance['anchor']
    color = instance['color']
    if self.size_mode == SizeMode.TARGET:
      target = instance['target']
    else:
      target = position + instance['vector']
    width = instance['width']
    bounds_width = target[0] - position[0]
    bounds_height = target[1] - position[1]
    shape = shapes.Line(
      x = position[0] + offset[0] - bounds_width * anchor[0],
      y = position[1] + offset[1] - bounds_height * anchor[1],
      x2 = target[0] + offset[0] - bounds_width * anchor[0],
      y2 = target[1] + offset[1] - bounds_height * anchor[1],
      width = width,
      color = color,
      batch = batch,
    )
    shape.opacity = 255 * instance['opacity']
    return shape
