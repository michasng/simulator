from simulator.visuals.plot.shape_groups.circle_group import CircleGroup
from simulator.visuals.plot.shape_groups.rectangle_group import RectangleGroup
from simulator.visuals.plot.shape_groups.square_group import SquareGroup
from simulator.visuals.plot.shape_groups.line_group import LineGroup
from simulator.visuals.plot.shape_groups.polygon_group import PolygonGroup
from simulator.visuals.plot.shape_groups.circle_section_group import CircleSectionGroup
from simulator.visuals.plot.shape_groups.text_group import TextGroup

shape_group_classes = {
  'circle': CircleGroup,
  'rectangle': RectangleGroup,
  'square': SquareGroup,
  'line': LineGroup,
  'polygon': PolygonGroup,
  'circle_section': CircleSectionGroup,
  'text': TextGroup,
}

def from_config(shape_config):
  cls = shape_group_classes[shape_config['type']]
  return cls(shape_config)
