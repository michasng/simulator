import os, sys
from simulator.config import get_app_config
from simulator.config.validator import read_json_dict, validate_json
from simulator.visuals.argument.argument import Argument
from simulator.visuals.argument.selector_mode import SelectorMode
from simulator.visuals.function.function_group import FunctionGroup
from simulator.visuals.plot.shape_groups import from_config as shape_group_from_config

class Prefab:
  def __init__(self, config, ref_config):
    self.config = config
    self.ref_config = ref_config
    # use SelectorMode.FIRST, becase prefabs are not prefab-groups
    self.selector = Argument.from_arg(ref_config['context'], default_mode = SelectorMode.FIRST)

    self.create_function_groups()
    self.create_shape_groups()

  def create_function_groups(self):
    self.function_groups = [FunctionGroup(function_config) for function_config in self.config['functions']]

  def run_function_groups(self, context):
    for function_group in self.function_groups:
      function_group.execute(context)

  def create_shape_groups(self):
    self.shape_groups = [shape_group_from_config(shape_config) for shape_config in self.config['shapes']]

  def create_context(self, outer_context):
    selected_context = self.selector.get(outer_context, as_value=True)
    self.run_function_groups(selected_context)
    return selected_context

  def init_state(self, context):
    prefab_context = self.create_context(context)
    for shape_group in self.shape_groups:
      shape_group.last_instances = shape_group.select_and_create_instances(prefab_context)

  def draw_keyframe(self, context, offset, batch):
    prefab_context = self.create_context(context)
    batch_memory = []
    for shape_group in self.shape_groups:
      b = shape_group.draw_keyframe(prefab_context, offset, batch)
      batch_memory.append(b)
    return batch_memory

  def draw_interpolated(self, progress, offset, batch):
    return [
      shape_group.draw_interpolated(progress, offset, batch)
      for shape_group in self.shape_groups
    ]

  @classmethod
  def from_config(cls, ref_config):
    file_name = ref_config['name'] + '.json'
    script_dir = os.path.dirname(__file__)
    prefabs_path = os.path.join(script_dir, '..', '..', 'samples', 'prefabs')
    config_path = os.path.join(prefabs_path, file_name)
    if os.path.isfile(config_path): # try to find in library
      config = read_json_dict(config_path)
      is_library_prefab = True
    else: # use working directory instead
      if not os.path.isfile(file_name):
        raise ValueError(f'Prefab {ref_config["name"]} does not exist.')
      config = read_json_dict(file_name)
      is_library_prefab = False
    
    try:
      validate_json(config, 'prefab.schema')
    except Exception as error:
      print(error)
      sys.exit(1)
    
    if ref_config["name"] != config['name']:
      raise ValueError(f'Conflicting prefab names: "{config["name"]}" in "{ref_config["name"]}.json".')

    if get_app_config()['debug']:
      if is_library_prefab:
        print(f'Prefab loaded (library): {config["name"]}.')
      else:
        print(f'Prefab loaded (project): {config["name"]}.')
    
    return Prefab(config, ref_config)
