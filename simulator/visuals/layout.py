import math

class GridCell:
  def __init__(self, width, height):
    self.width = width
    self.height = height
    self.owners = []

# sets the offset-values on the plots
def apply_layout(plots, border_size):
  min_x = min(plot.config['x'] for plot in plots)
  max_x = max(plot.config['x'] + plot.config['width'] for plot in plots)
  min_y = min(plot.config['y'] for plot in plots)
  max_y = max(plot.config['y'] + plot.config['height'] for plot in plots)
  x_range = max_x - min_x
  y_range = max_y - min_y

  for plot in plots:
    # subtracting min coords allows for negative numbers
    plot.offset = [plot.config['x'] - min_x + border_size, plot.config['y'] - min_y + border_size]

  return [x_range + 2 * border_size, y_range + 2 * border_size]
  