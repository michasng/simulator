# python modules are singletons, used here to acces config

_app_config = {}

def set_app_config(value):
    global _app_config
    _app_config = value

def get_app_config():
    return _app_config
