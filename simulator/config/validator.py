import json, os
import jsonschema
from jsonschema import validate
from jsonschema import Draft7Validator, validators, RefResolver

# snippet from docs:
# https://python-jsonschema.readthedocs.io/en/stable/faq/
def extend_with_default(validator_class):
  validate_properties = validator_class.VALIDATORS['properties']

  def set_defaults(validator, properties, instance, schema):
    for property, subschema in properties.items():
      if 'default' in subschema:
        instance.setdefault(property, subschema['default'])

    for error in validate_properties(
      validator, properties, instance, schema,
    ):
      yield error

  return validators.extend(
    validator_class, {'properties' : set_defaults},
  )
Validator = extend_with_default(Draft7Validator)

def read_json_dict(file_path):
  with open(file_path, 'r') as f:
    data = f.read()
  return json.loads(data)

def validate_json(data, schema_id):
  script_dir = os.path.dirname(__file__)
  app_schema_path = os.path.join(script_dir, 'app_schema.json')
  app_schema = read_json_dict(app_schema_path)
  function_schema_path = os.path.join(script_dir, 'visuals', 'function_schema.json')
  function_schema = read_json_dict(function_schema_path)
  shape_schema_path = os.path.join(script_dir, 'visuals', 'shape_schema.json')
  shape_schema = read_json_dict(shape_schema_path)
  prefab_schema_path = os.path.join(script_dir, 'visuals', 'prefab_schema.json')
  prefab_schema = read_json_dict(prefab_schema_path)
  schema_store = {
    app_schema['$id'] : app_schema,
    function_schema['$id'] : function_schema,
    shape_schema['$id'] : shape_schema,
    prefab_schema['$id']: prefab_schema,
  }
  resolver = RefResolver.from_schema(app_schema, store=schema_store)
  # validate(instance=data, schema=schema)
  Validator(schema_store[schema_id], resolver=resolver).validate(data)
