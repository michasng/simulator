import sys, inspect, os, json
import numpy as np
from simulator.core.np2json2np import EncodeFromNumpy, DecodeToNumpy
from simulator.config import set_app_config
from simulator.config.validator import read_json_dict, validate_json
from simulator.simulation.simulator import Simulator
from simulator.visuals.visualizer import Visualizer

def init_app_config():
  app_config = read_json_dict('config.json')
  try:
    validate_json(app_config, 'app.schema')
  except Exception as error:
    print(error)
    sys.exit(1)
  set_app_config(app_config) # keep config as a singleton
  return app_config

class NumpyEncoder(json.JSONEncoder):
  def default(self, obj):
    if isinstance(obj, np.ndarray):
      return obj.tolist()
    return json.JSONEncoder.default(self, obj)

def write_state_files(simulator, states, config, first_index = 0):
  if not config:
    return

  os.makedirs('states', exist_ok=True)
  for i, state in enumerate(states):
    state_index = i + first_index
    result = simulator.on_write_state_file(state_index, state)
    if result:
      continue
    state_str = json.dumps(state, indent = 2, cls=EncodeFromNumpy)
    with open(f'states\state{state_index}.json', 'w') as outfile:
      outfile.write(state_str)

def read_state_file(index):
  file_path = f'states\state{index}.json'    
  with open(file_path, 'r') as f:
    data = f.read()
  return json.loads(data, cls=DecodeToNumpy)    

def simulate(simulation, visualize = True):
  app_config = init_app_config()

  simulator = Simulator(app_config['simulation'], simulation)
  wsf_config = app_config['simulation']['write_state_files']

  if not visualize: # simulate without visuals
    new_states = simulator.run_all()
    write_state_files(simulator, new_states, wsf_config)
    return

  states = simulator.init() # list of simulation states
  write_state_files(simulator, states, wsf_config)
  fetch_state_index = 0 # next index to be fetched by the visuals

  def state_fetchable(): # as long as the last state has not been fetched
    return fetch_state_index < len(states) or simulator.missing_steps() > 0

  def fetch_state():
    # 'nonlocal' to allow writing the variable in local scope
    nonlocal fetch_state_index
    if fetch_state_index >= len(states): # might have to calculate new states
      new_states = simulator.update_batch()
      write_state_files(simulator, new_states, wsf_config, fetch_state_index)
      states.extend(new_states)
    fetched_state = states[fetch_state_index]
    fetch_state_index += 1
    return fetched_state

  def on_quit():
    simulator.quit()

  # using callbacks for loose coupling
  visualizer = Visualizer(app_config['visuals'], state_fetchable, fetch_state, on_quit)
  visualizer.run() # locks the thread

def visualize():
  app_config = init_app_config()

  fetch_state_index = 0
  def state_fetchable():
    return os.path.isfile(f'states\state{fetch_state_index}.json')

  def fetch_state():
    # 'nonlocal' to allow writing the variable in local scope
    nonlocal fetch_state_index
    state = read_state_file(fetch_state_index) 
    fetch_state_index += 1
    return state

  def on_quit():
    pass

  visualizer = Visualizer(app_config['visuals'], state_fetchable, fetch_state, on_quit)
  visualizer.run() # locks the thread
